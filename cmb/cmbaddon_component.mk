#
# Copyright (c) 2017, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# *  Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# *  Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# *  Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# File: cmbaddon_component.mk
#       This file is component include make file of cmb-addon library.
# List of variables set in this file and their purpose:
# <mod>_RELPATH        - This is the relative path of the module, typically from
#                        top-level directory of the package
# <mod>_PATH           - This is the absolute path of the module. It derives from
#                        absolute path of the top-level directory (set in env.mk)
#                        and relative path set above
# <mod>_INCLUDE        - This is the path that has interface header files of the
#                        module. This can be multiple directories (space separated)
# <mod>_PKG_LIST       - Names of the modules (and sub-modules) that are a part
#                        part of this module, including itself.
# <mod>_BOARD_DEPENDENCY - "yes": means the code for this module depends on
#                             board and the compiled obj/lib has to be kept
#                             under <board> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no board dependent code and hence
#                             the obj/libs are not kept under <board> dir.
# <mod>_CORE_DEPENDENCY     - "yes": means the code for this module depends on
#                             core and the compiled obj/lib has to be kept
#                             under <core> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no core dependent code and hence
#                             the obj/libs are not kept under <core> dir.
# <mod>_APP_STAGE_FILES     - List of source files that belongs to the module
#                             <mod>, but that needs to be compiled at application
#                             build stage (in the context of the app). This is
#                             primarily for link time configurations or if the
#                             source file is dependent on options/defines that are
#                             application dependent. This can be left blank or
#                             not defined at all, in which case, it means there
#                             no source files in the module <mod> that are required
#                             to be compiled in the application build stage.
#
ifeq ($(cmb_component_make_include), )

# under other list
drvcmb_BOARDLIST       = evmK2G evmOMAPL137
drvcmb_SOCLIST         = k2g omapl137
drvcmb_k2g_CORELIST    = c66x a15_0
drvcmb_omapl137_CORELIST    = c674x


############################
# uart package
# List of components included under uart lib
# The components included here are built and will be part of uart lib
############################
cmb_LIB_LIST = cmb cmb_profile cmb_indp cmb_profile_indp
drvcmb_LIB_LIST = $(cmb_LIB_LIST)

#
# CMB-ADDON Modules
#

# CMB-ADDON LIB
cmb_COMP_LIST = cmb
cmb_RELPATH = ti/addon/cmb
cmb_PATH = $(PDK_CMB_COMP_PATH)
cmb_LIBNAME = ti.addon.cmb
export cmb_LIBNAME
cmb_LIBPATH = $(cmb_PATH)/lib
export cmb_LIBPATH
cmb_OBJPATH = $(cmb_RELPATH)/cmb
export cmb_OBJPATH
cmb_MAKEFILE = -f build/makefile.mk
export cmb_MAKEFILE
cmb_BOARD_DEPENDENCY = no
cmb_CORE_DEPENDENCY = no
cmb_SOC_DEPENDENCY = yes
export cmb_COMP_LIST
export cmb_BOARD_DEPENDENCY
export cmb_CORE_DEPENDENCY
export cmb_SOC_DEPENDENCY
cmb_PKG_LIST = cmb
export cmb_PKG_LIST
cmb_INCLUDE = $(cmb_PATH)
cmb_SOCLIST = $(drvcmb_SOCLIST)
export cmb_SOCLIST
cmb_$(SOC)_CORELIST = $(drvcmb_$(SOC)_CORELIST)
export cmb_$(SOC)_CORELIST

# CMB-ADDON LIB DEVICE INDEPENDENT
cmb_indp_COMP_LIST = cmb_indp
cmb_indp_RELPATH = ti/addon/cmb
cmb_indp_PATH = $(PDK_CMB_COMP_PATH)
cmb_indp_LIBNAME = ti.addon.cmb
export cmb_indp_LIBNAME
cmb_indp_LIBPATH = $(cmb_indp_PATH)/lib
export cmb_indp_LIBPATH
cmb_indp_OBJPATH = $(cmb_indp_RELPATH)/cmb_indp
export cmb_indp_OBJPATH
cmb_indp_MAKEFILE = -f build/makefile_indp.mk
export cmb_indp_MAKEFILE
cmb_indp_BOARD_DEPENDENCY = no
cmb_indp_CORE_DEPENDENCY = no
cmb_indp_SOC_DEPENDENCY = no
export cmb_indp_COMP_LIST
export cmb_indp_BOARD_DEPENDENCY
export cmb_indp_CORE_DEPENDENCY
export cmb_indp_SOC_DEPENDENCY
cmb_indp_PKG_LIST = cmb_indp
cmb_indp_INCLUDE = $(cmb_indp_PATH)
cmb_indp_SOCLIST = $(drvcmb_SOCLIST)
export cmb_indp_SOCLIST
cmb_indp_$(SOC)_CORELIST = $(drvcmb_$(SOC)_CORELIST)
export cmb_indp_$(SOC)_CORELIST

# CMB-ADDON PROFILING SOC LIB
cmb_profile_COMP_LIST = cmb_profile
cmb_profile_RELPATH = ti/addon/cmb
cmb_profile_PATH = $(PDK_CMB_COMP_PATH)
cmb_profile_LIBNAME = ti.addon.cmb.profiling
export cmb_profile_LIBNAME
cmb_profile_LIBPATH = $(cmb_profile_PATH)/lib
export cmb_profile_LIBPATH
cmb_profile_OBJPATH = $(cmb_profile_RELPATH)/cmb_profile
export cmb_profile_OBJPATH
cmb_profile_MAKEFILE = -f build/makefile_profile.mk
export cmb_profile_MAKEFILE
cmb_profile_BOARD_DEPENDENCY = no
cmb_profile_CORE_DEPENDENCY = no
cmb_profile_SOC_DEPENDENCY = yes
export cmb_profile_COMP_LIST
export cmb_profile_BOARD_DEPENDENCY
export cmb_profile_CORE_DEPENDENCY
export cmb_profile_SOC_DEPENDENCY
cmb_profile_PKG_LIST = cmb_profile
cmb_profile_INCLUDE = $(cmb_profile_PATH)
cmb_profile_SOCLIST = $(drvcmb_SOCLIST)
export cmb_profile_SOCLIST
cmb_profile_$(SOC)_CORELIST = $(drvcmb_$(SOC)_CORELIST)
export cmb_profile_$(SOC)_CORELIST

# CMB-ADDON PROFILING SOC INDEPENDENT LIB
cmb_profile_indp_COMP_LIST = cmb_profile_indp
cmb_profile_indp_RELPATH = ti/addon/cmb
cmb_profile_indp_PATH = $(PDK_CMB_COMP_PATH)
cmb_profile_indp_LIBNAME = ti.addon.cmb.profiling
export cmb_profile_indp_LIBNAME
cmb_profile_indp_LIBPATH = $(cmb_profile_indp_PATH)/lib
export cmb_profile_indp_LIBPATH
cmb_profile_indp_OBJPATH = $(cmb_profile_indp_RELPATH)/cmb_profile_indp
export cmb_profile_indp_OBJPATH
cmb_profile_indp_MAKEFILE = -f build/makefile_profile_indp.mk
export cmb_profile_indp_MAKEFILE
cmb_profile_indp_BOARD_DEPENDENCY = no
cmb_profile_indp_CORE_DEPENDENCY = no
cmb_profile_indp_SOC_DEPENDENCY = no
export cmb_profile_indp_COMP_LIST
export cmb_profile_indp_BOARD_DEPENDENCY
export cmb_profile_indp_CORE_DEPENDENCY
export cmb_profile_indp_SOC_DEPENDENCY
cmb_profile_indp_PKG_LIST = cmb_profile_indp
cmb_profile_indp_INCLUDE = $(cmb_profile_indp_PATH)
cmb_profile_indp_SOCLIST = $(drvcmb_SOCLIST)
export cmb_profile_indp_SOCLIST
cmb_profile_indp_$(SOC)_CORELIST = $(drvcmb_$(SOC)_CORELIST)
export cmb_profile_indp_$(SOC)_CORELIST

export drvcmb_LIB_LIST
export cmb_LIB_LIST
export cmb_EXAMPLE_LIST

cmb_component_make_include := 1
endif
