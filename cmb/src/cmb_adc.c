/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      cmb_adc.c
 *
 *  \brief     Implementation of low level functions for PCM186x ADC.
 *
 *  This file contains low level library implementation for PCM186x ADC.
 *  This library is designed to work together with a high-level API layer
 *  which takes care of application interfacing. Input argument selection
 *  is done for ease of programming. Input argument boundary check and
 *  verification is expected to be done at the upper layers.
 *
 */

/******************************************************************************
 **                          INCLUDE FILE
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../cmb.h"
#if defined (SOC_K2G)
#include "evmK2G/include/evmc66x_audio_adc.h"
#include "evmK2G/include/evmc66x_i2c.h"
#endif // defined (SOC_K2G)

#if defined (SOC_OMAPL137)
#include "evmOMAPL137/include/evmc674x_audio_adc.h"
#include "evmOMAPL137/include/evmc674x_i2c.h"
#endif // defined (SOC_OMAPL137)
/******************************************************************************
 **                          GLOBAL DEFINITIONS
 ******************************************************************************/

#if (CMB_AUDIO_ADC)

/**
 * \brief     Array with the default values of ADC registers.
 *
 * {"Reg addr", "Value"}
 *
 * Array to to be used when we call cmb_pcm186xADCInit from app layer.
 *
 * Note: Default values shall be finalized while implementing audio demo
 *
 **/
#if defined (SOC_K2G)
static const CmbAdcRegDefConfig Pcm186xReg[] = {
	{0x00, 0x00}, /** ================ Changing to Page 0 ================ */

	///{0x01, 0x40}, /** PGA CH1_L to 32dB */
	///{0x02, 0x40}, /** PGA CH1_R to 32dB */
	///{0x03, 0x40}, /** PGA CH2_L to 32dB */
	///{0x04, 0x40}, /** PGA CH2_R to 32dB */
	///{0x05, 0x86}, /** SMOOTH : Smooth change,LINK : Independent control
	///		          DPGA_CLIP_EN : Disable,MAX_ATT : -3dB,START_ATT : 10,
	///		          AGC_EN : Disable */
	
	{0x06, 0x41}, /** Polarity : Normal, Channel : VINL1[SE] */
	{0x07, 0x41}, /** Polarity : Normal, Channel : VINR1[SE] */
	{0x08, 0x44}, /** Polarity : Normal, Channel : VINL3[SE] */
	{0x09, 0x44}, /** Polarity : Normal, Channel : VINR3[SE] */
	
	///{0x0A, 0x00}, /** Secondary ADC Input : No Selection */
	{0x0B, 0x44}, /** RX WLEN : 24bit, TDM_LRCK_MODE : Duty cycle of LRCK is
			          50%, TX WLEN : 24 bit, FMT : I2S format */
	
	{0x10, 0x03}, /** GPIO0_FUNC - SCK Out, GPIO0_POL - Normal
	                  GPIO1_FUNC - GPIO1, GPIO1_POL - Normal */
	{0x11, 0x50}, /** GPIO2_FUNC - GPIO2, GPIO2_POL - Normal
	                  GPIO3_FUNC - DOUT2, GPIO3_POL - Normal */
	{0x12, 0x04}, /** GPIO0_DIR - GPIO0 - Output
	                  GPIO1_DIR - GPIO1 - Input */
	{0x13, 0x40}, /** GPIO2_DIR - GPIO2 - Input
	                  GPIO3_DIR - GPIO3 - Output */
#if 0
	{0x20, 0x11}, /** SCK_XI_SEL : SCK or XTAL, MST_SCK_SRC : SCK or XI,
			          MST_MODE : Master, CMB_ADC_CLK_SRC : SCK,DSP2_CLK_SRC : SCK,
			          DSP1_CLK_SRC : SCK, CLKDET_EN : Disable */
	{0x26, 0x07}, /** SCK to BCK divider to 8 **/
#else
	///{0x29, 0x03}, // P Divide value = 1/4
	///{0x2a, 0x00}, // R Divide value = 1
	///{0x2b, 0x10}, // J Divide value = 16
	///{0x20, 0x3E}, // SCK_XI_SEL= SCK or Xtal, MST_SCK_SRC = PLL, MST_MODE= Master, ADC_CLK_SRC= PLL,
        ///          // DSP2_CLK_SRC=PLL, DSP1_CLK_SRC=PLL, CLKDET_EN=Disable
	///{0x21, 0x0B},  // PLL to DSP1 Divide value = 1/12
	///{0x22, 0x17},  // PLL to DSP2 Divide value = 1/24
	///{0x23, 0x2F},  // PLL to ADC Divide value = 1/48
	///{0x25, 0x17},  // PLL to SCK Divide value = 1/24
	///{0x26, 0x03}, /** SCK to BCK divider to 4 **/
	///{0x28, 0x01}, //PLL_REF_SEL=SCK; PLL_EN=Enabled
#endif

	///{0x60, 0x00}, /** POSTPGA : Disable,CLKERR : Disable,DC_CHANG : Disable,
	///		          DIN_TOGGLE : Disable,ENGSTR : Disable */
	///{0x71, 0x10}, /** 2CH : 4 Channels,FLT : Normal,HPF_EN : Enable,
	///		          Unmute all the Channles */
	///{0x00, 0x03}, /** ================ Changing to Page 3 =============== */
	///{0x12, 0x00}, /** Oscillator : Power up */
	///{0x15, 0x01}, /** Resistor bypass : Disable, Mic Bias: Power up */
	///{0x00, 0xFD}, /** ============== Changing to Page 255 =============== */
	///{0x14, 0x00}, /** PGA_ICI : 100%, REF_ICI : 100% */
	///{0x00, 0x00}  /** ================ Changing to Page 0 =============== */
};

static const CmbAdcRegDefConfig Pcm186xReg1[] = {
	{0x00, 0x00}, /** ================ Changing to Page 0 ================ */
	
	///{0x01, 0x40}, /** PGA CH1_L to 32dB */
	///{0x02, 0x40}, /** PGA CH1_R to 32dB */
	///{0x03, 0x40}, /** PGA CH2_L to 32dB */
	///{0x04, 0x40}, /** PGA CH2_R to 32dB */
	///{0x05, 0x86}, /** SMOOTH : Smooth change,LINK : Independent control
	///		          DPGA_CLIP_EN : Disable,MAX_ATT : -3dB,START_ATT : 10,
	///		          AGC_EN : Disable */
	
	{0x06, 0x41}, /** Polarity : Normal, Channel : VINL1[SE] */
	{0x07, 0x41}, /** Polarity : Normal, Channel : VINR1[SE] */
	{0x08, 0x44}, /** Polarity : Normal, Channel : VINL3[SE] */
	{0x09, 0x44}, /** Polarity : Normal, Channel : VINR3[SE] */
	
	///{0x0A, 0x00}, /** Secondary ADC Input : No Selection */
	{0x0B, 0x44}, /** RX WLEN : 24bit, TDM_LRCK_MODE : Duty cycle of LRCK is
			          50%, TX WLEN : 24 bit, FMT : I2S format */

	{0x10, 0x00}, /** GPIO0_FUNC - GPIO0, GPIO0_POL - Normal
	                  GPIO1_FUNC - GPIO1, GPIO1_POL - Normal */
	
	{0x11, 0x50}, /** GPIO2_FUNC - GPIO2, GPIO2_POL - Normal
	                  GPIO3_FUNC - DOUT2, GPIO3_POL - Normal */
	
	{0x12, 0x00}, /** GPIO0_DIR - GPIO0 - Intput
	                  GPIO1_DIR - GPIO1 - Input */
	{0x13, 0x40}, /** GPIO2_DIR - GPIO2 - Input
	                  GPIO3_DIR - GPIO3 - Output */
	{0x20, 0x01}, /** SCK_XI_SEL : SCK or XTAL, MST_SCK_SRC : SCK or XI,
			          MST_MODE : Slave, CMB_ADC_CLK_SRC : SCK,DSP2_CLK_SRC : SCK,
			          DSP1_CLK_SRC : SCK,CLKDET_EN : Enable */

	///{0x60, 0x00}, /** POSTPGA : Disable,CLKERR : Disable,DC_CHANG : Disable,
	///		          DIN_TOGGLE : Disable,ENGSTR : Disable */
	///{0x71, 0x10}, /** 2CH : 4 Channels,FLT : Normal,HPF_EN : Enable,
	///		          Unmute all the Channles */
	///{0x00, 0x03}, /** ================ Changing to Page 3 =============== */
	///{0x12, 0x00}, /** Oscillator : Power up */
	///{0x15, 0x01}, /** Resistor bypass : Disable, Mic Bias: Power up */
	///{0x00, 0xFD}, /** ============== Changing to Page 255 =============== */
	///{0x14, 0x00}, /** PGA_ICI : 100%, REF_ICI : 100% */
	///{0x00, 0x00}  /** ================ Changing to Page 0 =============== */
};
#endif // defined (SOC_K2G)

#if defined (SOC_OMAPL137)
static const CmbAdcRegDefConfig Pcm186xReg[] = {
	{0x00, 0x00}, /** ================ Changing to Page 0 ================ */
	
	{0x01, 0x40}, /** PGA CH1_L to 32dB */
	{0x02, 0x40}, /** PGA CH1_R to 32dB */
	{0x03, 0x40}, /** PGA CH2_L to 32dB */
	{0x04, 0x40}, /** PGA CH2_R to 32dB */
	///{0x05, 0x86}, /** SMOOTH : Smooth change,LINK : Independent control
	///		          DPGA_CLIP_EN : Disable,MAX_ATT : -3dB,START_ATT : 10,
	///		          AGC_EN : Disable */
	
	{0x06, 0x41}, /** Polarity : Normal, Channel : VINL1[SE] */
	{0x07, 0x41}, /** Polarity : Normal, Channel : VINR1[SE] */
	{0x08, 0x44}, /** Polarity : Normal, Channel : VINL3[SE] */
	{0x09, 0x44}, /** Polarity : Normal, Channel : VINR3[SE] */
	
	///{0x0A, 0x00}, /** Secondary ADC Input : No Selection */
	{0x0B, 0x44}, /** RX WLEN : 24bit, TDM_LRCK_MODE : Duty cycle of LRCK is
			          50%, TX WLEN : 24 bit, FMT : I2S format */

	{0x10, 0x00}, /** GPIO0_FUNC - GPIO0, GPIO0_POL - Normal
	                  GPIO1_FUNC - GPIO1, GPIO1_POL - Normal */
	
	{0x11, 0x50}, /** GPIO2_FUNC - GPIO2, GPIO2_POL - Normal
	                  GPIO3_FUNC - DOUT2, GPIO3_POL - Normal */
	
	{0x12, 0x00}, /** GPIO0_DIR - GPIO0 - Intput
	                  GPIO1_DIR - GPIO1 - Input */
	{0x13, 0x40}, /** GPIO2_DIR - GPIO2 - Input
	                  GPIO3_DIR - GPIO3 - Output */
	{0x20, 0x41}, /** SCK_XI_SEL : SCK or XTAL, MST_SCK_SRC : SCK or XI,
			          MST_MODE : Slave, CMB_ADC_CLK_SRC : SCK,DSP2_CLK_SRC : SCK,
			          DSP1_CLK_SRC : SCK,CLKDET_EN : Enable */

	///{0x60, 0x00}, /** POSTPGA : Disable,CLKERR : Disable,DC_CHANG : Disable,
	///		          DIN_TOGGLE : Disable,ENGSTR : Disable */
	///{0x71, 0x10}, /** 2CH : 4 Channels,FLT : Normal,HPF_EN : Enable,
	///		          Unmute all the Channles */
	///{0x00, 0x03}, /** ================ Changing to Page 3 =============== */
	///{0x12, 0x00}, /** Oscillator : Power up */
	///{0x15, 0x01}, /** Resistor bypass : Disable, Mic Bias: Power up */
	///{0x00, 0xFD}, /** ============== Changing to Page 255 =============== */
	///{0x14, 0x00}, /** PGA_ICI : 100%, REF_ICI : 100% */
	///{0x00, 0x00}  /** ================ Changing to Page 0 =============== */
};

static const CmbAdcRegDefConfig Pcm186xReg1[] = {
	{0x00, 0x00}, /** ================ Changing to Page 0 ================ */
	
	{0x01, 0x40}, /** PGA CH1_L to 32dB */
	{0x02, 0x40}, /** PGA CH1_R to 32dB */
	{0x03, 0x40}, /** PGA CH2_L to 32dB */
	{0x04, 0x40}, /** PGA CH2_R to 32dB */
	///{0x05, 0x86}, /** SMOOTH : Smooth change,LINK : Independent control
	///		          DPGA_CLIP_EN : Disable,MAX_ATT : -3dB,START_ATT : 10,
	///		          AGC_EN : Disable */
	
	{0x06, 0x41}, /** Polarity : Normal, Channel : VINL1[SE] */
	{0x07, 0x41}, /** Polarity : Normal, Channel : VINR1[SE] */
	{0x08, 0x44}, /** Polarity : Normal, Channel : VINL3[SE] */
	{0x09, 0x44}, /** Polarity : Normal, Channel : VINR3[SE] */
	
	///{0x0A, 0x00}, /** Secondary ADC Input : No Selection */
	{0x0B, 0x44}, /** RX WLEN : 24bit, TDM_LRCK_MODE : Duty cycle of LRCK is
			          50%, TX WLEN : 24 bit, FMT : I2S format */

	{0x10, 0x00}, /** GPIO0_FUNC - GPIO0, GPIO0_POL - Normal
	                  GPIO1_FUNC - GPIO1, GPIO1_POL - Normal */
	
	{0x11, 0x50}, /** GPIO2_FUNC - GPIO2, GPIO2_POL - Normal
	                  GPIO3_FUNC - DOUT2, GPIO3_POL - Normal */
	
	{0x12, 0x00}, /** GPIO0_DIR - GPIO0 - Intput
	                  GPIO1_DIR - GPIO1 - Input */
	{0x13, 0x40}, /** GPIO2_DIR - GPIO2 - Input
	                  GPIO3_DIR - GPIO3 - Output */
	{0x20, 0x41}, /** SCK_XI_SEL : SCK or XTAL, MST_SCK_SRC : SCK or XI,
			          MST_MODE : Slave, CMB_ADC_CLK_SRC : SCK,DSP2_CLK_SRC : SCK,
			          DSP1_CLK_SRC : SCK,CLKDET_EN : Enable */

	///{0x60, 0x00}, /** POSTPGA : Disable,CLKERR : Disable,DC_CHANG : Disable,
	///		          DIN_TOGGLE : Disable,ENGSTR : Disable */
	///{0x71, 0x10}, /** 2CH : 4 Channels,FLT : Normal,HPF_EN : Enable,
	///		          Unmute all the Channles */
	///{0x00, 0x03}, /** ================ Changing to Page 3 =============== */
	///{0x12, 0x00}, /** Oscillator : Power up */
	///{0x15, 0x01}, /** Resistor bypass : Disable, Mic Bias: Power up */
	///{0x00, 0xFD}, /** ============== Changing to Page 255 =============== */
	///{0x14, 0x00}, /** PGA_ICI : 100%, REF_ICI : 100% */
	///{0x00, 0x00}  /** ================ Changing to Page 0 =============== */
};
#endif // defined (SOC_OMAPL137)

/******************************************************************************
 **                          FUNCTION DEFINITIONS
 ******************************************************************************/
char write_buffer[MAX_WRITE_LEN];
void cmb_write(const char *fmt, ... )
{
	va_list 	arg_ptr;

	/* Initial platform_write to temporary buffer.. at least try some sort of sanity check so we don't write all over
	 * memory if the print is too large.
	 */
	if (strlen(fmt) > MAX_WRITE_LEN) {return;}

	va_start( arg_ptr, fmt );
	vsprintf( (char *)write_buffer, fmt, arg_ptr );
	va_end( arg_ptr );

	printf( "%s", write_buffer );
	//fflush(stdout);

	return;
}

/**
 * \brief     Reads ADC register.
 *
 * \param     addr [IN] ADC HW instance I2C slave address.
 * \param     reg  [IN] Register to be written.
 * \param     data [IN] Data to be written
 *
 * \return    0 for success.
 *
 **/
static CMB_ADC_RET cmb_pcm186x_read_reg(Uint8 addr, Uint8 reg, Uint8 *data)
{
	CMB_ADC_RET ret;

	DBG_PCM186x (cmb_write("\nEnter cmb_pcm186x_read_reg\n"));

	ret = cmb_i2cRead(CMB_PCM186x_I2C_PORT_NUM, addr, data, reg, 1, 1);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186x_read_reg() : cmb_i2cRead error : ret = %d\n", ret));
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186x_read_reg() : ADC Read Successful:  Reg Addr = 0x%x;  Reg Value = 0x%x\n", reg, *data));
	}

	DBG_PCM186x (cmb_write("Exit cmb_pcm186x_read_reg\n"));

	return (ret);
}

/**
 * \brief     Writes into ADC register.
 *
 * Note: Disable ENABLE_CMB_ADC_REG_ECHO to stop register echo by write function
 *
 * \param     addr [IN] ADC HW instance I2C slave address.
 * \param     reg  [IN] Register to be written.
 * \param     data [IN] Data to be written
 *
 * \return    0 for success.
 *
 **/
static CMB_ADC_RET cmb_pcm186x_write_reg(Uint8 addr, Uint8 reg, Uint8 data)
{
	Uint8 slaveData[2];
	CMB_ADC_RET ret;
	Uint8 value;

	slaveData[0] = reg;
	slaveData[1] = data;

	DBG_PCM186x (cmb_write("\nEnter cmb_pcm186x_write_reg\n"));

	DBG_PCM186x (cmb_write("\n Write Request : addr = 0x%x reg = 0x%x"
				    " data = 0x%x page = %d\n", addr, reg, data,
				    cmb_pcm186xPageCheck(addr)));

	ret = cmb_i2cWrite(CMB_PCM186x_I2C_PORT_NUM, addr, slaveData, 2, 0);
	if(ret)
	{
		IFPRINT (cmb_write("\ncmb_pcm186x_write_reg(): cmb_i2cWrite error : ret = %d\n", ret));
	}

#ifdef ENABLE_CMB_ADC_REG_ECHO
	ret = cmb_pcm186x_read_reg(addr, reg, &value);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186x_write_reg(): ADC Read for Reg Echo Failed\n"));
	}
#endif

	DBG_PCM186x (cmb_write("Exit cmb_pcm186x_write_reg\n"));

	return (ret);
}

/**
 * \brief     Sets all ADC registers to the default value.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_pcm186xAdcInit(Uint8 addr)
{
	CMB_ADC_RET ret = -1;
	Uint8   count;

	if (addr==CMB_AUDIO_ADC0_ADDR)
	{
		DBG_PCM186x (cmb_write("\nEnter cmb_pcm186xAdcInit\n"));

		DBG_PCM186x (cmb_write("cmb_pcm186xAdcInit() : addr = 0x%x page = %d\n",
						addr, cmb_pcm186xPageCheck(addr)));

		for (count = 0; count < ARRAY_SIZE(Pcm186xReg); count++)
		{
			ret = cmb_pcm186x_write_reg(addr, Pcm186xReg[count].reg, Pcm186xReg[count].def);
			if(ret)
			{
				IFPRINT (cmb_write("cmb_pcm186xAdcInit() : Error in Writing Register = 0x%x\n",
										Pcm186xReg[count].reg));
				return (ret);
			}
		}

		DBG_PCM186x (cmb_write("cmb_pcm186xAdcInit() : In Page %d\n.", cmb_pcm186xPageCheck(addr)));

		DBG_PCM186x (cmb_write("Exit cmb_pcm186xAdcInit\n"));
	} else
	{
		DBG_PCM186x (cmb_write("\nEnter cmb_pcm186xAdcInit\n"));

		DBG_PCM186x (cmb_write("cmb_pcm186xAdcInit() : addr = 0x%x page = %d\n",
						addr, cmb_pcm186xPageCheck(addr)));

		for (count = 0; count < ARRAY_SIZE(Pcm186xReg1); count++)
		{
			ret = cmb_pcm186x_write_reg(addr, Pcm186xReg1[count].reg, Pcm186xReg1[count].def);
			if(ret)
			{
				IFPRINT (cmb_write("cmb_pcm186xAdcInit() : Error in Writing Register = 0x%x\n",
										Pcm186xReg1[count].reg));
				return (ret);
			}
		}

		DBG_PCM186x (cmb_write("cmb_pcm186xAdcInit() : In Page %d\n.", cmb_pcm186xPageCheck(addr)));

		DBG_PCM186x (cmb_write("Exit cmb_pcm186xAdcInit\n"));
	}
	return (ret);
}

/**
 * \brief     Reads the Current Page no.
 *
 * \param     addr [IN] ADC HW instance I2C slave address.
 *
 * \return    Page no if success.
 *
 **/
Int8 cmb_pcm186xPageCheck(Uint8 addr)
{
	Int8  ret;
	Uint8 read = 0;

	DBG_PCM186x (cmb_write("Enter cmb_pcm186xPageCheck\n"));

	ret = cmb_pcm186x_read_reg(addr, 0x0, &read);
	if(ret)
	{
		IFPRINT (cmb_write("Error in reading Register 0 "
					"ret = %d\n", ret));
		return (ret);
	}

	DBG_PCM186x (cmb_write("Exit cmb_pcm186xPageCheck\n"));

	return (read);
}

/**
 * \brief     Register dump of Page 0, 1 and 253.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_pcm186xRegDump(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8 count;
	Uint8 read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xRegDump() : addr = 0x%x\n", addr));

	/* Page 0 Dump */
	ret = cmb_pcm186x_write_reg(addr, 0x0, 0x00);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xRegDump() : ADC Page Switch Failed\n"));
		return (ret);
	}

	/* Some of the register addresses in page 0 are not defined.
	   Not sure whether they can be accessed or not.
	   Need to change this in case of issues */
	for (count = 1; count <= 120; count++)
	{
		ret = cmb_pcm186x_read_reg(addr, count, &read);
		if(ret)
		{
			IFPRINT (cmb_write("cmb_pcm186xRegDump() : Error in Reading Register = 0x%x\n",
									count));
			return (ret);
		}

		DBG_PCM186x (cmb_write("Page %d : Register 0x%x = 0x%x\n",
							cmb_pcm186xPageCheck(addr), count, read));
		read = 0;
	}

	/* Page 1 Dump */
	ret = cmb_pcm186x_write_reg(addr, 0x0, 0x01);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xRegDump() : ADC Page Switch Failed\n"));
		return (ret);
	}

	for (count = 1; count <= 11; count++)
	{
		ret = cmb_pcm186x_read_reg(addr, count, &read);
		if(ret)
		{
			IFPRINT (cmb_write("cmb_pcm186xRegDump() : Error in Reading Register = 0x%x\n",
									count));
			return (ret);
		}

		DBG_PCM186x (cmb_write("Page %d : Register 0x%x = 0x%x\n",
					    cmb_pcm186xPageCheck(addr), count, read));
		read = 0;
	}

	/* Page 3 Dump */
	ret = cmb_pcm186x_write_reg(addr, 0x0, 0x03);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xRegDump() : ADC Page Switch Failed\n"));
		return (ret);
	}

	ret = cmb_pcm186x_read_reg(addr, 0x12, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xRegDump() : Error in Reading Register = 0x%x\n",
								0x12));
		return (ret);
	}

	DBG_PCM186x (cmb_write("Page %d : Register 0x12 = 0x%x\n",
				    cmb_pcm186xPageCheck(addr), read));
	read = 0;
	ret = cmb_pcm186x_read_reg(addr, 0x15, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xRegDump() : Error in Reading Register = 0x%x\n",
								0x15));
		return (ret);
	}

	DBG_PCM186x (cmb_write("Page %d : Register 0x15 = 0x%x\n",
				    cmb_pcm186xPageCheck(addr), read));
	read = 0;

	/* Page 253 Dump */
	ret = cmb_pcm186x_write_reg(addr, 0x0, 0xFD);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xRegDump() : ADC Page Switch Failed\n"));
		return (ret);
	}

	ret = cmb_pcm186x_read_reg(addr, 0x14, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xRegDump() : Error in Reading Register = 0x%x\n",
								0x14));
		return (ret);
	}

	DBG_PCM186x (cmb_write("Page %d : Register 0x14 = 0x%x\n",
				    cmb_pcm186xPageCheck(addr), read));

    /* Change back to Page 0 */
	ret = cmb_pcm186x_write_reg(addr, 0x0, 0x00);

	return (ret);
}

/**
 * \brief     Enable/Disable Mic Bias Control for analog MIC input.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \param     power   [IN] Mic bias control
 *                         1 - Power up mic bias
 *                         0 - Power down mic bias
 *
 * \return    0 if success.
 *
 */
CMB_ADC_RET cmb_pcm186xMicBiasCtrl(Uint8 addr, Uint8 power)
{
	Uint8   read = 0;
	CMB_ADC_RET ret;

	/* Changing to Page 3 */
	ret = cmb_pcm186x_write_reg(addr, 0x0, 0x03);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xMicBiasCtrl() : ADC Page Switch Failed\n"));
		return (ret);
	}

	DBG_PCM186x (cmb_write("cmb_pcm186xMicBiasCtrl() : addr = 0x%x"
				    " power = %d page = %d\n", addr, power,
				    cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_MIC_BIAS_CTRL, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xMicBiasCtrl() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_MIC_BIAS_CTRL));
		return (ret);
	}

	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_MIC_BIAS_CTRL, ((read & 0xFE) | power));
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xMicBiasCtrl() : Error in Writing Register = 0x%x\n",
		                        CMB_PCM186x_MIC_BIAS_CTRL));
		return (ret);
	}

    /* Changing to Page 0 */
	ret = cmb_pcm186x_write_reg(addr, 0x0, 0x00);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xMicBiasCtrl() : ADC Page Switch Failed\n"));
		return (ret);
	}

	DBG_PCM186x (cmb_write("cmb_pcm186xMicBiasCtrl() : In page %d.\n",
	                            cmb_pcm186xPageCheck(addr)));

	return (ret);
}

/**
 * \brief     Resets PCM1865 ADC.                         .
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_cmb_pcm186xReset(Uint8 addr)
{
	CMB_ADC_RET ret;

	DBG_PCM186x (cmb_write("cmb_cmb_pcm186xReset() : addr = 0x%x page = %d\n",
				    addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_ADC_RESET, 0xFF);

	return (ret);
}

/**
 * \brief     Set PLL for PCM1865 ADC.                         .
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_cmb_pcm186xSetPLL(Uint8 addr)
{
	CMB_ADC_RET ret;

	DBG_PCM186x (cmb_write("cmb_cmb_pcm186xSetPLL() : addr = 0x%x page = %d\n",
				    addr, cmb_pcm186xPageCheck(addr)));

	// {0x00, 0x00} set to page 0
	ret = cmb_pcm186x_write_reg(addr, 0x00, 0x00);
#if 0
	// {0x20, 0x3E}
	ret |= cmb_pcm186x_write_reg(addr, 0x20, 0x3E);
	// {0x21, 0x0B},  // PLL to DSP1 Divide value = 1/12
	ret |= cmb_pcm186x_write_reg(addr, 0x21, 0x0B);
	// {0x22, 0x17},  // PLL to DSP2 Divide value = 1/24
	ret |= cmb_pcm186x_write_reg(addr, 0x22, 0x17);
	// {0x23, 0x2F},  // PLL to ADC Divide value = 1/48
	ret |= cmb_pcm186x_write_reg(addr, 0x23, 0x2F);
#else
	// {0x20, 0x30}
	ret |= cmb_pcm186x_write_reg(addr, 0x20, 0x30);
	// {0x21, 0x02},  // XTAL to DSP1 Divide value = 1/3
	ret |= cmb_pcm186x_write_reg(addr, 0x21, 0x02);
	// {0x22, 0x05},  // XTAL to DSP2 Divide value = 1/6
	ret |= cmb_pcm186x_write_reg(addr, 0x22, 0x05);
	// {0x23, 0x0B},  // XTAL to ADC Divide value = 1/12
	ret |= cmb_pcm186x_write_reg(addr, 0x23, 0x0B);
#endif
	// {0x25, 0x17},  // PLL to SCK Divide value = 1/24
	ret |= cmb_pcm186x_write_reg(addr, 0x25, 0x17);
	// {0x26, 0x03}, /** SCK to BCK divider to 4 **/
	ret |= cmb_pcm186x_write_reg(addr, 0x26, 0x03);

	// {0x29, 0x03} P Divide value = 1/4
	ret |= cmb_pcm186x_write_reg(addr, 0x29, 0x03);
	// {0x2a, 0x00} R Divide value = 1
	ret |= cmb_pcm186x_write_reg(addr, 0x2a, 0x00);
	// {0x2b, 0x10} J Divide value = 16
	ret |= cmb_pcm186x_write_reg(addr, 0x2b, 0x10);
	// {0x28, 0x01} PLL_REF_SEL=SCK; PLL_EN=Enabled
	ret |= cmb_pcm186x_write_reg(addr, 0x28, 0x01);

	return (ret);
}

/**
 * \brief     Configures the data format and slot width
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     dataType [IN] Data type for the codec operation
 *                           CMB_ADC_DATA_FORMAT_I2S     - for I2S mode
 *                           CMB_ADC_DATA_FORMAT_LEFTJ   - for left aligned data
 *                           CMB_ADC_DATA_FORMAT_RIGHTJ  - for right aligned data
 *                           CMB_ADC_DATA_FORMAT_TDM_DSP - for TDM/DSP data.
 *
 * \param     slotWidth [IN] Slot width in bits
 *                           CMB_ADC_RX_WLEN_24BIT - 24 bit
 *                           CMB_ADC_RX_WLEN_20BIT - 20 bit
 *                           CMB_ADC_RX_WLEN_16BIT - 16 bit
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_pcm186xDataConfig(Uint8 addr, Uint8 dataType, Uint8 slotWidth)
{
	Uint8   read = 0;
	Uint8   val;
	CMB_ADC_RET ret;

	DBG_PCM186x (cmb_write("cmb_pcm186xDataConfig() : addr = 0x%x dataType = %d "
				    "slotWidth = %d page = %d\n", addr, dataType,
				    slotWidth, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_AUDIO_FMT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xDataConfig() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_AUDIO_FMT));
		return (ret);
	}

	val = (dataType | (slotWidth << 6));

	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_AUDIO_FMT, ((read & 0x3C) | val));

	return (ret);
}

/**
 * \brief     Selects input channel for ADC.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \param     channel [IN] Channel selection
 *                      0 - ADC CH1 LEFT
 *                      1 - ADC CH1 RIGHT
 *                      2 - ADC CH2 LEFT
 *                      3 - ADC CH2 RIGHT
 *
 * \param     input   [IN] Input selection
 *                      0x0  : No Select
 *                      0x1  : VINL1[SE]
 *                      0x2  : VINL2[SE]
 *                      0x3  : VINL2[SE] + VINL1[SE]
 *                      0x4  : VINL3[SE]
 *                      0x5  : VINL3[SE] + VINL1[SE]
 *                      0x6  : VINL3[SE] + VINL2[SE]
 *                      0x7  : VINL3[SE] + VINL2[SE] + VINL1[SE]
 *                      0x8  : VINL4[SE]
 *                      0x9  : VINL4[SE] + VINL1[SE]
 *                      0xA  : VINL4[SE] + VINL2[SE]
 *                      0xB  : VINL4[SE] + VINL2[SE] + VINL1[SE]
 *                      0xC  : VINL4[SE] + VINL3[SE]
 *                      0xD  : VINL4[SE] + VINL3[SE] + VINL1[SE]
 *                      0xE  : VINL4[SE] + VINL3[SE] + VINL2[SE]
 *                      0xF  : VINL4[SE] + VINL3[SE] + VINL2[SE] + VINL1[SE]
 *                      0x10 : {VIN1P, VIN1M}[DIFF]
 *                      0x20 : {VIN4P, VIN4M}[DIFF]
 *                      0x30 : {VIN1P, VIN1M}[DIFF] + {VIN4P, VIN4M}[DIFF]
 *
 * \return   0 for success.
 *
 **/
CMB_ADC_RET cmb_cmb_pcm186xInputSel(Uint8 addr, Uint8 channel, Uint8 input)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_cmb_pcm186xInputSel() : addr = 0x%x "
				                "channel = %d input = 0x%x page = %d\n",
				                addr, channel, input, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_INPUT_SELECT(channel), &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_cmb_pcm186xInputSel() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_INPUT_SELECT(channel)));
		return (ret);
	}

    read = ((read & 0xC0) | input);
	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_INPUT_SELECT(channel), read);

	return (ret);
}

/**
 * \brief     Sets the ADC PGA Volume.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \param     vol     [IN] Volume in percentage; 0 to 100
 *
 * \param     channel [IN] Channel selection mask
 *                         CMB_ADC_CH1_LEFT - ADC CH1 LEFT
 *                         CMB_ADC_CH1_RIGHT - ADC CH1 RIGHT
 *                         CMB_ADC_CH2_LEFT - ADC CH2 LEFT
 *                         CMB_ADC_CH2_RIGHT - ADC CH2 RIGHT
 *                         CMB_ADC_CH_ALL - All the four channels
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_cmb_pcm186xSetVolume(Uint8 addr, Uint8 vol, Uint8 channel)
{
	Uint8   value;
	Uint8   count;
	CMB_ADC_RET ret;

	DBG_PCM186x (cmb_write("cmb_cmb_pcm186xSetVolume() : addr = 0x%x vol = %d "
				    "channel = %d page = %d\n", addr,
				    vol, channel, cmb_pcm186xPageCheck(addr)));

	/* Gains -12dB to 38dB are supported with step value of 0.5.
	   Values from 38.5dB to 40dB are ignored to simplify the computation.
	   Volume percentage 0 to 24 indicates -12dB to 0dB and
	   Volume percentage 25 to 100 indicates 0.5dB to 38dB */
	if(vol <= 24)
	{
		value = (24 - vol)/2;

		/* ADC datasheet shows same 7 bit value for -0.5 and -1.
		   Adding 1 to add values may be needed to satisfy this condition */
		if(vol % 2)
		{
			value += 1;
		}

		if(value)
		{
			/* Calculate 2's compliment */
			value = ~(value) + 1;
			value <<= 1;
		}
	}
	else
	{
		value = (vol - 24)/2;
		value <<= 1;
	}

	if(vol % 2)
	{
		value = value | 0x1;
	}

	if(channel == CMB_ADC_CH_ALL)
	{
		for (count = CMB_ADC_CH1_LEFT; count <= CMB_ADC_CH2_RIGHT; count++)
		{
			ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_VOL_CTRL(count), value);
			if(ret)
			{
				IFPRINT (cmb_write("cmb_cmb_pcm186xSetVolume() : Error in Writing Register = 0x%x\n",
										CMB_PCM186x_VOL_CTRL(count)));
				return (ret);
			}
		}
	}
	else
	{
		ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_VOL_CTRL(channel), value);
	}

	return (ret);
}

/**
 * \brief     Unmute or Mute ADC the Channel.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \param     channel [IN] Channel selection mask
 *                         0 - ADC CH1 LEFT
 *                         2 - ADC CH1 RIGHT
 *                         4 - ADC CH2 LEFT
 *                         8 - ADC CH2 RIGHT
 *                         0xF - All the four channels
 *
 * \param     mute    [IN] Mute control
 *                         1 - Mute the channel
 *                         0 - Unmute the channel
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xMuteChannel(Uint8 addr, Uint8 channel, Uint8 mute)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xMuteChannel() : addr = 0x%x mute = %d"
				    " channel = %d page = %d\n", addr, mute,
				    channel, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_MUTE_CTRL, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xMuteChannel() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_MUTE_CTRL));
		return (ret);
	}

	if(mute == 0)
	{
		read = (read & ~(channel));
	}
	else
	{
		read = (read | channel);
	}

	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_MUTE_CTRL, read);

	return (ret);
}

/**
 * \brief     Unmute or Mute ADC the Channel.
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     powState [IN] ADC power state
 *                     CMB_ADC_POWER_STATE_STANDBY - ADC standby state
 *                     CMB_ADC_POWER_STATE_SLEEP - ADC device sleep state
 *                     CMB_ADC_POWER_STATE_POWERDOWN - ADC Analog Power Down state
 *
 * \param     enable  [IN] Mute control
 *                         1 - Enables the power state
 *                         0 - Disables the power state
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xConfigPowState(Uint8 addr, Uint8 powState, Uint8 enable)
{
	CMB_ADC_RET ret;
	Uint8 read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xConfigPowState() : addr = 0x%x"
				    " powState = %d enable = %d page = %d\n",
				    addr, powState, enable,
				    cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_POWER_STATE_SEL, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xConfigPowState() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_POWER_STATE_SEL));
		return (ret);
	}

	read = (read & ~(1 << powState)) | (enable << powState);

	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_POWER_STATE_SEL, read);

	return (ret);
}

/**
 * \brief     Enables/Disables ADC interrupts.
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     intrNum  [IN] ADC interrupt ID
 *                   CMB_ADC_INTR_ENERGY_SENSE - Energysense Interrupt
 *                   CMB_ADC_INTR_DIN_TOGGLE - I2S RX DIN toggle Interrupt
 *                   CMB_ADC_INTR_DC_CHANGE - DC Level Change Interrupt
 *                   CMB_ADC_INTR_CLK_ERR - Clock Error Interrupt
 *                   CMB_ADC_INTR_POST_PGA_CLIP - Post-PGA Clipping Interrupt
 *                   CMB_ADC_INTR_ALL - To controls all the ADC interrupts together
 *
 * \param     enable   [IN] Interrupt control
 *                          1 - Enables the interrupt
 *                          0 - Disables the interrupt
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xSetIntr(Uint8 addr, Uint8 intrNum, Uint8 enable)
{
	CMB_ADC_RET ret;
	Uint8   value;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xSetIntr() : addr = 0x%x"
				    " intrNum = %d enable = %d page = %d read ="
				    " 0x%x", addr, intrNum, enable,
				    cmb_pcm186xPageCheck(addr), read));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_INTR_SEL, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xSetIntr() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_INTR_SEL));
		return (ret);
	}

	if(intrNum == CMB_ADC_INTR_ALL)
	{
		if(enable)
		{
			value = 0x1F;
		}
		else
		{
			value = 0x00;
		}

		read = ((read & 0xE0) | value);
	}
	else
	{
		read = ((read & (~(1 << intrNum))) | (enable << intrNum));
	}

	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_INTR_SEL, read);

	return (ret);
}

/**
 * \brief     Reads ADC interrupt status register.
 *
 * \param     addr [IN] ADC HW instance I2C slave address.
 *
 * \return    Value of interrupt status register in case of success
 *            0xFF in case of failure
 *
 **/
Uint8 cmb_pcm186xGetIntrStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetIntrStatus() : addr = 0x%x page"
				    " = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_INTR_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetIntrStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_INTR_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetIntrStatus() : Intr Status = 0x%x\n", read));
		return (read);
	}
}

/**
 * \brief     Controls ADC DSP channel configuration
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     channel  [IN] Channel configuration
 *                       CMB_ADC_DSP_PROC_4CHAN - ADC DSP 4 channel mode processing
 *                       CMB_ADC_DSP_PROC_2CHAN - ADC DSP 2 channel mode processing
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xDspCtrl(Uint8 addr, Uint8 channel)
{
	CMB_ADC_RET ret;
	Uint8   read;

	DBG_PCM186x (cmb_write("cmb_pcm186xDspCtrl() : addr = 0x%x channel = %d"
				    " page = %d\n", addr, channel,
				    cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_MUTE_CTRL, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xDspCtrl() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_MUTE_CTRL));
		return (ret);
	}

	read = ((read & 0x7F) | (channel << 7));

	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_MUTE_CTRL, read);

	return (ret);
}

/**
 * \brief     Programs ADC DSP coefficients
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     regAddr  [IN] DSP register address
 *
 * \param     coeff    [IN] DSP coefficient
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xProgDspCoeff(Uint8 addr, Uint8 regAddr, Uint32 coeff)
{
	CMB_ADC_RET ret;
	Uint8   value;
	Uint8   count;
	Uint8   coeffBuf[3];

	DBG_PCM186x (cmb_write("cmb_pcm186xProgDspCoeff() : addr = 0x%x "
				    "Coeff Reg = 0x%x  Coeff = 0x%x page = %d\n",
				    addr, regAddr, coeff, cmb_pcm186xPageCheck(addr)));

	/* Switch to page 1 */
	ret = cmb_pcm186x_write_reg(addr, 0x0, 0x1);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xProgDspCoeff() : ADC Page Switch Failed\n"));
		return (ret);
	}

	/* Write the memory address of coefficient register */
	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_DSP_MEM_ADDR, regAddr);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xProgDspCoeff() : Error in Writing Register = 0x%x\n",
		                        CMB_PCM186x_DSP_MEM_ADDR));
		return (ret);
	}

	coeffBuf[0] = (coeff >> 16) & 0xFF;
	coeffBuf[1] = (coeff >> 8) & 0xFF;
	coeffBuf[2] = (coeff & 0xFF);

	/* Write the coefficient data */
	for (count = 0; count < 3; count ++)
	{
		ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_DSP_MEM_WDATA(count),
		                        coeffBuf[count]);
		if(ret)
		{
			IFPRINT (cmb_write("cmb_pcm186xProgDspCoeff() : Error in Writing Register = 0x%x\n",
									CMB_PCM186x_DSP_MEM_WDATA(count)));
			return (ret);
		}
	}

	/* Execute write operation */
	ret = cmb_pcm186x_write_reg(addr, CMB_PCM186x_DSP_PROG, 0x1);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xProgDspCoeff() : Error in Writing Register = 0x%x\n",
		                        CMB_PCM186x_DSP_PROG));
		return (ret);
	}

	/* Wait for DSP coefficient write to complete */
	do
	{
		ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_DSP_PROG, &value);
		if(ret)
		{
			IFPRINT (cmb_write("cmb_pcm186xProgDspCoeff() : Error in Reading Register = 0x%x\n",
									CMB_PCM186x_DSP_PROG));
			return (ret);
		}
	} while (!(value & 0x10)); //TODO: Need to confirm the status check bit

	/* Switch to page 0 */
	ret = cmb_pcm186x_write_reg(addr, 0, 0x0);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xProgDspCoeff() : ADC Page Switch Failed\n"));
		return (ret);
	}

	return (ret);
}

/**
 * \brief     Reads ADC power state
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0x0 - Power Down
 *            0x1 - Wait clock stable
 *            0x2 - Release reset
 *            0x3 - Stand-by
 *            0x4 - Fade IN
 *            0x5 - Fade OUT
 *            0x9 - Sleep
 *            0xF - Run
 *
 **/
Uint8 cmb_pcm186xGetPowerStateStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetPowerStateStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_POWER_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetPowerStateStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_POWER_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetPowerStateStatus() : read = 0x%x\n", read));
		return (read & 0x0F);
	}
}

/**
 * \brief     Reads current sampling frequency
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0x0 - Out of range (Low) or LRCK Halt
 *            0x1 - 8kHz
 *            0x2 - 16kHz
 *            0x3 - 32-48kHz
 *            0x4 - 88.2-96kHz
 *            0x5 - 176.4-192kHz
 *            0x6 - Out of range (High)
 *            0x7 - Invalid Fs
 *
 **/
Uint8 cmb_pcm186xGetSampleFreqStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetSampleFreqStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, PCM16x_SAMPLE_FREQ_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetSampleFreqStatus() : Error in Reading Register = 0x%x\n",
		                        PCM16x_SAMPLE_FREQ_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetSampleFreqStatus() : read = 0x%x\n", read));
		return (read & 0x07);
	}
}

/**
 * \brief     Reads bit clock ratio status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0x0 - Out of range (L) or BCK Halt
 *            0x1 - 32
 *            0x2 - 48
 *            0x3 - 64
 *            0x4 - 256
 *            0x6 - Out of range (High)
 *            0x7 - Invalid BCK ratio or LRCK Halt
 *
 **/
Uint8 cmb_pcm186xGetBckRatioStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetBckRatioStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_BCK_SCK_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetBckRatioStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_BCK_SCK_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetBckRatioStatus() : read = 0x%x\n", read));
		return ((read & 0x70) >> 4);
	}
}

/**
 * \brief     Reads Current SCK Ratio
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0x0 - Out of range (L) or SCK Halt
 *            0x1 - 128
 *            0x2 - 256
 *            0x3 - 384
 *            0x4 - 512
 *            0x5 - 768
 *            0x6 - Out of range (High)
 *            0x7 - Invalid SCK ratio or LRCK Halt
 *
 **/
Uint8 cmb_pcm186xGetSckRatioStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetSckRatioStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_BCK_SCK_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetSckRatioStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_BCK_SCK_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("read = 0x%x\n", read));
		return (read & 0x07);
	}
}

/**
 * \brief     Reads LRCK Halt Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Halt
 *
 **/
Uint8 cmb_pcm186xGetLrckHltStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8 read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetLrckHltStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_CLK_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetLrckHltStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_CLK_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetLrckHltStatus() : read = 0x%x\n", read));
		return (CMB_EXTRACT_STATUS(read, LRCKHLT));
	}
}

/**
 * \brief     Reads BCK Halt Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Halt
 *
 **/
Uint8 cmb_pcm186xGetBckHltStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetBckHltStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_CLK_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetBckHltStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_CLK_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetBckHltStatus() : read = 0x%x\n", read));
		return (CMB_EXTRACT_STATUS(read, BCKHLT));
	}
}

/**
 * \brief     Reads SCK Halt Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Halt
 *
 **/
Uint8 cmb_pcm186xGetSckHltStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetSckHltStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_CLK_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetSckHltStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_CLK_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetSckHltStatus() : read = 0x%x\n", read));
		return (CMB_EXTRACT_STATUS(read, SCKHLT));
	}
}

/**
 * \brief     Reads LRCK Error Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Error
 *
 **/
Uint8 cmb_pcm186xGetLrckErrStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetLrckErrStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_CLK_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetLrckErrStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_CLK_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetLrckErrStatus() : read = 0x%x\n", read));
		return (CMB_EXTRACT_STATUS(read, LRCKERR));
	}
}

/**
 * \brief     Reads BCK Error Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Error
 *
 **/
Uint8 cmb_pcm186xGetBckErrStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetBckErrStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_CLK_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetBckErrStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_CLK_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetBckErrStatus() : read = 0x%x\n", read));
		return (CMB_EXTRACT_STATUS(read, BCKERR));
	}
}

/**
 * \brief     Reads SCK Error Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Error
 *
 **/
Uint8 cmb_pcm186xGetSckErrStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetSckErrStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_CLK_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetSckErrStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_CLK_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetSckErrStatus() : read = 0x%x\n", read));
		return (CMB_EXTRACT_STATUS(read, SCKERR));
	}
}

/**
 * \brief     Reads DVDD Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - Bad/Missing
 *            1 - Good
 *
 **/
Uint8 cmb_pcm186xGetDvddStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetDvddStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_VOLTAGE_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetDvddStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_VOLTAGE_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetDvddStatus() : read = 0x%x\n", read));
		return (CMB_EXTRACT_STATUS(read, DVDD));
	}
}

/**
 * \brief     Reads AVDD Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - Bad/Missing
 *            1 - Good
 *
 **/
Uint8 cmb_pcm186xGetAvddStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetAvddStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_VOLTAGE_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetAvddStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_VOLTAGE_STAT));
		return (0xFF);
	}
	else {
		DBG_PCM186x (cmb_write("cmb_pcm186xGetAvddStatus() : read = 0x%x\n", read));
		return (CMB_EXTRACT_STATUS(read, AVDD));
	}
}

/**
 * \brief     Reads Digital LDO Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - Bad/Missing
 *            1 - Good
 *
 **/
Uint8 cmb_pcm186xGetLdoStatus(Uint8 addr)
{
	CMB_ADC_RET ret;
	Uint8   read = 0;

	DBG_PCM186x (cmb_write("cmb_pcm186xGetLdoStatus() : addr = 0x%x"
				    " page = %d\n", addr, cmb_pcm186xPageCheck(addr)));

	ret = cmb_pcm186x_read_reg(addr, CMB_PCM186x_VOLTAGE_STAT, &read);
	if(ret)
	{
		IFPRINT (cmb_write("cmb_pcm186xGetLdoStatus() : Error in Reading Register = 0x%x\n",
		                        CMB_PCM186x_VOLTAGE_STAT));
		return (0xFF);
	}
	else
	{
		DBG_PCM186x (cmb_write("cmb_pcm186xGetLdoStatus() : read = 0x%x\n", read));
		return (CMB_EXTRACT_STATUS(read, LDO));
	}
}

#endif /* #if (CMB_AUDIO_ADC) */

/***************************** End Of File ***********************************/
