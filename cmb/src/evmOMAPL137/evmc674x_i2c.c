/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file  evm674x_i2c.c
 *
 * \brief This contains C66x specific i2c functions.
 *
 ******************************************************************************/

 /* I2C modules */
#include "../../cmb.h"
#include "include/evmc674x_i2c.h"
#include "include/codec_if.h"

#define CUSTOM_DELAY	0x100

#if (PLATFORM_I2C_IN)
#endif

uint32_t cmb_errno2 = 0;

///extern unsigned int I2C_slaveAddr;

/******************************************************************************
 *
 * Function:	cmb_i2cWrite
 *
 * Description:	Enters master transmitter mode, writes a specified number
 *              of bytes to the given slave address.
 *
 * Note: It is expected that address offset of the I2C slave device to write
 * data is part of pData in the format expected by the device.
 *
 * Parameters:	uint8_t  i2cPortNumber - The i2c port number
 *              uint8_t  slaveAddress  - i2c slave device address
 * 				uint8_t *pData         - Pointer to the buffer base address
 * 				uint32_t numBytes      - Number of bytes of buffer
 * 				uint32_t endBusState   - The state on which bus should be left
 *
 * Return Value: I2C_RET - status
 *
 ******************************************************************************/
I2C_RET cmb_i2cWrite( uint8_t i2cPortNumber, uint8_t slaveAddress, uint8_t *pData,
		          uint32_t numBytes, uint32_t endBusState)
{
  	I2C_RET     returnValue;

	IFPRINT_I2CDEBUG(cmb_write("cmb_i2cWrite: "
	                                "I2C Port Number - %d   "
	                                "I2C Slave Address - 0x%x   "
	                                "Bytes to Write - %d   "
	                                "End Bus State - %d\n",
	                                i2cPortNumber, slaveAddress,
	                                numBytes, endBusState));

	if (numBytes>2)
	{
		returnValue = I2C_RET_INVALID_PARAM;
	} else
	{
		if (i2cPortNumber==CSL_I2C_0)
		{
			///I2C_slaveAddr = slaveAddress;
			CodecRegWrite(CSL_I2C_0_DATA_CFG, pData[0], pData[1]);
			returnValue = I2C_RET_OK;
		} else
		{
			returnValue = I2C_RET_INVALID_PARAM;
		}
	}

  	return (returnValue);

} // cmb_i2cWrite

/******************************************************************************
 *
 * Function:	cmb_i2cRead
 *
 * Description:	Reads a fixed number of bytes from an I2C device. The read
 *              consists of a master write of slave address (maximum 4 bytes)
 *              followed by a master read of the input number of bytes.
 *
 * Parameters:	uint8_t i2cPortNumber - The i2c port number
 *              uint8_t slaveAddress  - i2c slave device address
 * 				uint8_t *pData        - Pointer to the buffer base address
 * 				uint8_t readOffset    - Offset to read from
 * 				uint8_t OffsetLen     - Length of read offset in bytes (1 to 4)
 * 				uint32_t numBytes     - Number of bytes of buffer
 *
 * Return Value: I2C_RET - status
 *
 ******************************************************************************/
I2C_RET cmb_i2cRead ( uint8_t i2cPortNumber, uint8_t slaveAddress, uint8_t *pData,
                  uint32_t readOffset, uint32_t OffsetLen, uint32_t numBytes)
{
  	I2C_RET     returnValue;

	IFPRINT_I2CDEBUG(cmb_write("cmb_i2cRead: "
	                                "I2C Port Number - %d   "
	                                "I2C Slave Address - %d   "
	                                "Bytes to Read - %d   "
	                                "Read Offset - %d   "
	                                "Read Offset Length - %d\n",
	                                i2cPortNumber, slaveAddress,
	                                numBytes, readOffset, OffsetLen));

	if (numBytes>2)
	{
		returnValue = I2C_RET_INVALID_PARAM;
	} else
	{
		if (i2cPortNumber==CSL_I2C_0)
		{
			///I2C_slaveAddr = slaveAddress;
			pData[1] = CodecRegRead(CSL_I2C_0_DATA_CFG, pData[0]);
			returnValue = I2C_RET_OK;
		} else
		{
			returnValue = I2C_RET_INVALID_PARAM;
		}
	}

  	return (returnValue);

} // cmb_i2cRead

void cmb_delaycycles(uint32_t cycles)
{
	uint32_t start_val  = CSL_chipReadTSCL();

	while ((CSL_chipReadTSCL() - start_val) < cycles);

	return;
}

/******************************************************************************
 *
 * Function:	cmb_i2cDelay
 *
 * Description:	Creates a delay.
 *
 * Parameters:	uint32_t count - Counter for the loop
 *
 * Return Value: void
 *
 ******************************************************************************/
void cmb_i2cDelay (uint32_t count)
{
	uint32_t i;

  	for (i = 0; i < count; i++)
  	{
		cmb_delaycycles(50000);
	}
}  //cmb_i2cDelay

/* Nothing past this point */

