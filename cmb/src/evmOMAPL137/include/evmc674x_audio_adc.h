/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      evmc674x_audio_dc_adc.h
 *
 * \brief     Audio ADC low level library header file.
 *
 *  This file contains PCM186x ADC specific structure, typedefs and
 *  function prototypes.
 *
 */

#ifndef _EVMC674X_AUDIO_DC_CMB_ADC_H_
#define _EVMC674X_AUDIO_DC_CMB_ADC_H_

/**************************************************************************
 **                       Macro Definitions
 **************************************************************************/

/** Page 0 Register Definitions */
#define CMB_PCM186x_ADC_RESET       (0x00)
#define CMB_PCM186x_PGA_VAL_CH1_L   (0x01)
#define CMB_PCM186x_PGA_VAL_CH1_R   (0x02)
#define CMB_PCM186x_PGA_VAL_CH2_L   (0x03)
#define CMB_PCM186x_PGA_VAL_CH2_R   (0x04)
#define CMB_PCM186x_ADC_CH1_L       (0x06)
#define CMB_PCM186x_ADC_CH1_R       (0x07)
#define CMB_PCM186x_ADC_CH2_L       (0x08)
#define CMB_PCM186x_ADC_CH2_R       (0x09)
#define CMB_PCM186x_SEC_CMB_ADC_CH      (0x0A)
#define CMB_PCM186x_AUDIO_FMT       (0x0B)
#define CMB_PCM186x_DIG_MIC_CTRL    (0x1A)
#define CMB_PCM186x_SCK_XI_SEL      (0x20)
#define CMB_PCM186x_INTR_SEL        (0x60)
#define CMB_PCM186x_INTR_STAT       (0x61)
#define CMB_PCM186x_POWER_STATE_SEL (0x70)
#define CMB_PCM186x_MUTE_CTRL       (0x71)
#define CMB_PCM186x_POWER_STAT      (0x72)
#define PCM16x_SAMPLE_FREQ_STAT (0x73)
#define CMB_PCM186x_BCK_SCK_STAT    (0x74)
#define CMB_PCM186x_CLK_STAT        (0x75)
#define CMB_PCM186x_VOLTAGE_STAT    (0x78)

#define CMB_PCM186x_DSP_PROG          (0x1)
#define CMB_PCM186x_DSP_MEM_ADDR      (0x2)
#define CMB_PCM186x_DSP_MEM_WDATA(n)  (0x4 + n)
#define CMB_PCM186x_DSP_MEM_RDATA(n)  (0x8 + n)


/** Page 3 Register Definitions */
#define CMB_PCM186x_MIC_BIAS_CTRL (0x15)

/** Function to calculate DAC input selection register address */
#define CMB_PCM186x_INPUT_SELECT(x)  (CMB_PCM186x_ADC_CH1_L + x)
/** Function to calculate DAC volume control register address */
#define CMB_PCM186x_VOL_CTRL(x)      (CMB_PCM186x_PGA_VAL_CH1_L + x)

/** Macro for I2C Port Number */
#define CMB_PCM186x_I2C_PORT_NUM (I2C_PORT_0)

#define CMB_EXTRACT_STATUS(read, FIELD)  \
	(((read) & CMB_PCM186x_##FIELD##_MASK) >> CMB_PCM186x_##FIELD##_SHIFT)

/** Macros for Shift and Masking of Status Registers */
#define CMB_PCM186x_LRCKHLT_SHIFT (6)
#define CMB_PCM186x_BCKHLT_SHIFT  (5)
#define CMB_PCM186x_SCKHLT_SHIFT  (4)
#define CMB_PCM186x_LRCKERR_SHIFT (2)
#define CMB_PCM186x_BCKERR_SHIFT  (1)
#define CMB_PCM186x_SCKERR_SHIFT  (0)
#define CMB_PCM186x_DVDD_SHIFT    (2)
#define CMB_PCM186x_AVDD_SHIFT    (1)
#define CMB_PCM186x_LDO_SHIFT     (0)

#define CMB_PCM186x_LRCKHLT_MASK  (1 << CMB_PCM186x_LRCKHLT_SHIFT)
#define CMB_PCM186x_BCKHLT_MASK   (1 << CMB_PCM186x_BCKHLT_SHIFT)
#define CMB_PCM186x_SCKHLT_MASK   (1 << CMB_PCM186x_SCKHLT_SHIFT)
#define CMB_PCM186x_LRCKERR_MASK  (1 << CMB_PCM186x_LRCKERR_SHIFT)
#define CMB_PCM186x_BCKERR_MASK   (1 << CMB_PCM186x_BCKERR_SHIFT)
#define CMB_PCM186x_SCKERR_MASK   (1 << CMB_PCM186x_SCKERR_SHIFT)
#define CMB_PCM186x_DVDD_MASK     (1 << CMB_PCM186x_DVDD_SHIFT)
#define CMB_PCM186x_AVDD_MASK     (1 << CMB_PCM186x_AVDD_SHIFT)
#define CMB_PCM186x_LDO_MASK      (1 << CMB_PCM186x_LDO_SHIFT)

/** Macro to enable DEBUG mode */
#define DEBUG_CMB_PCM186x_ENABLE

/** Macro to enable register echo in cmb_pcm186x_write_reg()
    When this macro is enabled, every register written is read back and
    its value is displayed by cmb_pcm186x_write_reg()*/
#define ENABLE_CMB_ADC_REG_ECHO (1)

#ifdef DEBUG_CMB_PCM186x_ENABLE
#define DBG_PCM186x(x) IFPRINT(x)
#else
#define DBG_PCM186x(x)
#endif

/** ADC API return type */
typedef Int32 CMB_ADC_RET;

/**************************************************************************
 **                       Structure Definitions
 **************************************************************************/

/**
 * \brief Structure to store register
 *         default values.
 */
typedef struct _CmbAdcRegDefConfig
{
	Uint8 reg;
	Uint8 def;

} CmbAdcRegDefConfig;


/**************************************************************************
 **                      API function Prototypes
 **************************************************************************/

/**
 * \brief     Sets all ADC registers to the default value.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_pcm186xAdcInit(Uint8 addr);

/**
 * \brief     Set PLL for PCM1865 ADC.                         .
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_cmb_pcm186xSetPLL(Uint8 addr);

/**
 * \brief     Reads the Current Page no.
 *
 * \param     addr [IN] ADC HW instance I2C slave address.
 *
 * \return    Page no if success.
 *
 **/
Int8 cmb_pcm186xPageCheck(Uint8 addr);

/**
 * \brief     Register dump of Page 0, 1 and 253.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_pcm186xRegDump(Uint8 addr);

/**
 * \brief     Enable/Disable Mic Bias Control for analog MIC input.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \param     power   [IN] Mic bias control
 *                         1 - Power up mic bias
 *                         0 - Power down mic bias
 *
 * \return    0 if success.
 *
 */
CMB_ADC_RET cmb_pcm186xMicBiasCtrl(Uint8 addr, Uint8 power);

/**
 * \brief     Resets PCM1865 ADC.                         .
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_cmb_pcm186xReset(Uint8 addr);

/**
 * \brief     Configures the data format and slot width
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     dataType [IN] Data type for the codec operation
 *                           CMB_ADC_DATA_FORMAT_I2S     - for I2S mode
 *                           CMB_ADC_DATA_FORMAT_LEFTJ   - for left aligned data
 *                           CMB_ADC_DATA_FORMAT_RIGHTJ  - for right aligned data
 *                           CMB_ADC_DATA_FORMAT_TDM_DSP - for TDM/DSP data.
 *
 * \param     slotWidth [IN] Slot width in bits
 *                           CMB_ADC_RX_WLEN_24BIT - 24 bit
 *                           CMB_ADC_RX_WLEN_20BIT - 20 bit
 *                           CMB_ADC_RX_WLEN_16BIT - 16 bit
 *
 * \return    0 if success.
 *
 **/
CMB_ADC_RET cmb_pcm186xDataConfig(Uint8 addr, Uint8 dataType, Uint8 slotWidth);

/**
 * \brief     Selects input channel for ADC.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \param     channel [IN] Channel selection
 *                      0 - ADC CH1 LEFT
 *                      1 - ADC CH1 RIGHT
 *                      2 - ADC CH2 LEFT
 *                      3 - ADC CH2 RIGHT
 *
 * \param     input   [IN] Input selection
 *                      0x0  : No Select
 *                      0x1  : VINL1[SE]
 *                      0x2  : VINL2[SE]
 *                      0x3  : VINL2[SE] + VINL1[SE]
 *                      0x4  : VINL3[SE]
 *                      0x5  : VINL3[SE] + VINL1[SE]
 *                      0x6  : VINL3[SE] + VINL2[SE]
 *                      0x7  : VINL3[SE] + VINL2[SE] + VINL1[SE]
 *                      0x8  : VINL4[SE]
 *                      0x9  : VINL4[SE] + VINL1[SE]
 *                      0xA  : VINL4[SE] + VINL2[SE]
 *                      0xB  : VINL4[SE] + VINL2[SE] + VINL1[SE]
 *                      0xC  : VINL4[SE] + VINL3[SE]
 *                      0xD  : VINL4[SE] + VINL3[SE] + VINL1[SE]
 *                      0xE  : VINL4[SE] + VINL3[SE] + VINL2[SE]
 *                      0xF  : VINL4[SE] + VINL3[SE] + VINL2[SE] + VINL1[SE]
 *                      0x10 : {VIN1P, VIN1M}[DIFF]
 *                      0x20 : {VIN4P, VIN4M}[DIFF]
 *                      0x30 : {VIN1P, VIN1M}[DIFF] + {VIN4P, VIN4M}[DIFF]
 *
 * \return   0 for success.
 *
 **/
CMB_ADC_RET cmb_cmb_pcm186xInputSel(Uint8 addr, Uint8 channel, Uint8 input);

/**
 * \brief     Sets the ADC PGA Volume.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \param     vol     [IN] Volume in percentage; 0 to 100
 *
 * \param     channel [IN] Channel selection mask
 *                         CMB_ADC_CH1_LEFT - ADC CH1 LEFT
 *                         CMB_ADC_CH1_RIGHT - ADC CH1 RIGHT
 *                         CMB_ADC_CH2_LEFT - ADC CH2 LEFT
 *                         CMB_ADC_CH2_RIGHT - ADC CH2 RIGHT
 *                         CMB_ADC_CH_ALL - All the four channels
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_cmb_pcm186xSetVolume(Uint8 addr, Uint8 vol, Uint8 channel);

/**
 * \brief     Unmute or Mute ADC the Channel.
 *
 * \param     addr    [IN] ADC HW instance I2C slave address.
 *
 * \param     channel [IN] Channel selection mask
 *                         0 - ADC CH1 LEFT
 *                         2 - ADC CH1 RIGHT
 *                         4 - ADC CH2 LEFT
 *                         8 - ADC CH2 RIGHT
 *                         0xF - All the four channels
 *
 * \param     mute    [IN] Mute control
 *                         1 - Mute the channel
 *                         0 - Unmute the channel
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xMuteChannel(Uint8 addr, Uint8 channel, Uint8 mute);

/**
 * \brief     Unmute or Mute ADC the Channel.
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     powState [IN] ADC power state
 *                     CMB_ADC_POWER_STATE_STANDBY - ADC standby state
 *                     CMB_ADC_POWER_STATE_SLEEP - ADC device sleep state
 *                     CMB_ADC_POWER_STATE_POWERDOWN - ADC Analog Power Down state
 *
 * \param     enable  [IN] Mute control
 *                         1 - Enables the power state
 *                         0 - Disables the power state
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xConfigPowState(Uint8 addr, Uint8 powState, Uint8 enable);

/**
 * \brief     Enables/Disables ADC interrupts.
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     intrNum  [IN] ADC interrupt ID
 *                   CMB_ADC_INTR_ENERGY_SENSE - Energysense Interrupt
 *                   CMB_ADC_INTR_DIN_TOGGLE - I2S RX DIN toggle Interrupt
 *                   CMB_ADC_INTR_DC_CHANGE - DC Level Change Interrupt
 *                   CMB_ADC_INTR_CLK_ERR - Clock Error Interrupt
 *                   CMB_ADC_INTR_POST_PGA_CLIP - Post-PGA Clipping Interrupt
 *                   CMB_ADC_INTR_ALL - To controls all the ADC interrupts together
 *
 * \param     enable   [IN] Interrupt control
 *                          1 - Enables the interrupt
 *                          0 - Disables the interrupt
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xSetIntr(Uint8 addr, Uint8 intrNum, Uint8 enable);

/**
 * \brief     Reads ADC interrupt status register.
 *
 * \param     addr [IN] ADC HW instance I2C slave address.
 *
 * \return    Value of interrupt status register in case of success
 *            0xFF in case of failure
 *
 **/
Uint8 cmb_pcm186xGetIntrStatus(Uint8 addr);

/**
 * \brief     Controls ADC DSP channel configuration
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     channel  [IN] Channel configuration
 *                       CMB_ADC_DSP_PROC_4CHAN - ADC DSP 4 channel mode processing
 *                       CMB_ADC_DSP_PROC_2CHAN - ADC DSP 2 channel mode processing
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xDspCtrl(Uint8 addr, Uint8 channel);

/**
 * \brief     Programs ADC DSP coefficients
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \param     regAddr  [IN] DSP register address
 *
 * \param     coeff    [IN] DSP coefficient
 *
 * \return    0 for success.
 *
 **/
CMB_ADC_RET cmb_pcm186xProgDspCoeff(Uint8 addr, Uint8 regAddr, Uint32 coeff);

/**
 * \brief     Reads ADC power state
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0x0 - Power Down
 *            0x1 - Wait clock stable
 *            0x2 - Release reset
 *            0x3 - Stand-by
 *            0x4 - Fade IN
 *            0x5 - Fade OUT
 *            0x9 - Sleep
 *            0xF - Run
 *
 **/
Uint8 cmb_pcm186xGetPowerStateStatus(Uint8 addr);

/**
 * \brief     Reads current sampling frequency
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0x0 - Out of range (Low) or LRCK Halt
 *            0x1 - 8kHz
 *            0x2 - 16kHz
 *            0x3 - 32-48kHz
 *            0x4 - 88.2-96kHz
 *            0x5 - 176.4-192kHz
 *            0x6 - Out of range (High)
 *            0x7 - Invalid Fs
 *
 **/
Uint8 cmb_pcm186xGetSampleFreqStatus(Uint8 addr);

/**
 * \brief     Reads bit clock ratio status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0x0 - Out of range (L) or BCK Halt
 *            0x1 - 32
 *            0x2 - 48
 *            0x3 - 64
 *            0x4 - 256
 *            0x6 - Out of range (High)
 *            0x7 - Invalid BCK ratio or LRCK Halt
 *
 **/
Uint8 cmb_pcm186xGetBckRatioStatus(Uint8 addr);

/**
 * \brief     Reads Current SCK Ratio
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0x0 - Out of range (L) or SCK Halt
 *            0x1 - 128
 *            0x2 - 256
 *            0x3 - 384
 *            0x4 - 512
 *            0x5 - 768
 *            0x6 - Out of range (High)
 *            0x7 - Invalid SCK ratio or LRCK Halt
 *
 **/
Uint8 cmb_pcm186xGetSckRatioStatus(Uint8 addr);

/**
 * \brief     Reads LRCK Halt Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Halt
 *
 **/
Uint8 cmb_pcm186xGetLrckHltStatus(Uint8 addr);

/**
 * \brief     Reads BCK Halt Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Halt
 *
 **/
Uint8 cmb_pcm186xGetBckHltStatus(Uint8 addr);

/**
 * \brief     Reads SCK Halt Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Halt
 *
 **/
Uint8 cmb_pcm186xGetSckHltStatus(Uint8 addr);

/**
 * \brief     Reads LRCK Error Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Error
 *
 **/
Uint8 cmb_pcm186xGetLrckErrStatus(Uint8 addr);

/**
 * \brief     Reads BCK Error Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Error
 *
 **/
Uint8 cmb_pcm186xGetBckErrStatus(Uint8 addr);

/**
 * \brief     Reads SCK Error Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - No Error
 *            1 - Error
 *
 **/
Uint8 cmb_pcm186xGetSckErrStatus(Uint8 addr);

/**
 * \brief     Reads DVDD Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - Bad/Missing
 *            1 - Good
 *
 **/
Uint8 cmb_pcm186xGetDvddStatus(Uint8 addr);

/**
 * \brief     Reads AVDD Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - Bad/Missing
 *            1 - Good
 *
 **/
Uint8 cmb_pcm186xGetAvddStatus(Uint8 addr);

/**
 * \brief     Reads Digital LDO Status
 *
 * \param     addr     [IN] ADC HW instance I2C slave address.
 *
 * \return    0xFF for failure or below values for success.
 *            0 - Bad/Missing
 *            1 - Good
 *
 **/
Uint8 cmb_pcm186xGetLdoStatus(Uint8 addr);

#endif // _EVMC674X_AUDIO_DC_CMB_ADC_H_


/***************************** End Of File ***********************************/
