#
# This file is the makefile for building CMB RTOS library.
#
#   Copyright (c) Texas Instruments Incorporated 2017
#

SRCDIR += . src/$(BOARD)
INCDIR += . src/$(BOARD)/include

# Source files for $(BOARD)
ifeq ($(BOARD),$(filter $(BOARD), evmOMAPL137))
SRCS_COMMON += evmc674x_pinmux.c
SRCS_COMMON += evmc674x_i2c.c
SRCS_COMMON += codec_if.c


PACKAGE_SRCS_COMMON += makefile include/cmb.h cmb_component.mk \
                      src/$(BOARD)/evmc674x_pinmux.c \
                      src/$(BOARD)/evmc674x_i2c.c \
                      src/$(BOARD)/codec_if.c \
	 	      build/makefile.mk \
	 	      src/$(BOARD)/src_file.mk                      
endif

