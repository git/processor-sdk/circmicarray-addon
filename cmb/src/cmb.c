/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      cmb_audio.c
 *
 * \brief     Platform audio interface file.
 *
 * This file contains APIs for accessing DAC/ADC/DIR modules on audio daughter
 * card.
 *
 */

#include "../cmb.h"
#if defined (SOC_K2G)
#include "evmK2G/include/evmc66x_pinmux.h"
#include "evmK2G/include/evmc66x_i2c.h"
#include "evmK2G/include/evmc66x_gpio.h"
#include "evmK2G/include/evmc66x_audio_adc.h"
#include "evmK2G/include/evmc66x_audio_dac.h"

#if (CMB_AUDIO)

/* DAC HW instance I2C slave address */
Uint8 cmb_gDacI2cAddr[CMB_AUDIO_DAC_COUNT] = {CMB_AUDIO_DAC0_ADDR,
                                               CMB_AUDIO_DAC1_ADDR};
/* ADC HW instance I2C slave address */
Uint8 cmb_gAdcI2cAddr[CMB_AUDIO_CMB_ADC_COUNT] = {CMB_AUDIO_ADC0_ADDR,
                                               CMB_AUDIO_ADC1_ADDR};

uint32_t cmb_errno = 0;

/******************************************************************************
 * cmb_delay
 ******************************************************************************/
#define K2G_C66X_MHZ (600)
Cmb_STATUS cmb_delay(uint32_t usecs)
{
	int32_t delayCount = (int32_t) usecs * K2G_C66X_MHZ;
	int32_t start_val  = (int32_t) CSL_chipReadTSCL();

	while (((int32_t)CSL_chipReadTSCL() - start_val) < delayCount);

	return Cmb_EOK;
}
                                               
/**
 *  \brief    Initializes Audio module
 *
 *  This function configures the system level setup required for
 *  operation of the modules that are available on audio daughter card.
 *  This function shall be called before calling any other platform
 *  audio module functions.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioInit(void)
{
	uint8_t cmb_padCfg;
	uint8_t cmb_padMax;

	IFPRINT(cmb_write("cmb_AudioInit Called \n"));

	/* Configure McASP0 lines */
	/* MCASP0AMUTE, MCASP0ACLKR, MCASP0FSR, MCASP0AHCLKR
       MCASP0ACLKX, MCASP0FSX, MCASP0AHCLKX,
       MCASP0AXR[0:7] - PADCONFIG 169 to 183 */
    cmb_padMax = 183;
	for (cmb_padCfg = 169; cmb_padCfg <= cmb_padMax; cmb_padCfg++)
	{
		cmb_pinMuxSetMode(cmb_padCfg, CMB_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		cmb_pullSetMode(cmb_padCfg, 0);
	}

	/* MCASP0AXR[12:15] - PADCONFIG 188 to 191 */
    cmb_padMax = 191;
	for (cmb_padCfg = 188; cmb_padCfg <= cmb_padMax; cmb_padCfg++)
	{
		cmb_pinMuxSetMode(cmb_padCfg, CMB_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		cmb_pullSetMode(cmb_padCfg, 0);
	}


	/* Configure McASP1 lines */
	/* MCASP1ACLKR, MCASP1FSR, MCASP1AHCLKR - PADCONFIG 152 to 154 */
    cmb_padMax = 154;
	for (cmb_padCfg = 152; cmb_padCfg <= cmb_padMax; cmb_padCfg++)
	{
		cmb_pinMuxSetMode(cmb_padCfg, CMB_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		cmb_pullSetMode(cmb_padCfg, 0);
	}

	/* MCASP1AHCLKX - PADCONFIG 157 */
	cmb_pinMuxSetMode(157, CMB_PADCONFIG_MUX_MODE_QUINARY);
	// set to weak pull down
	cmb_pullSetMode(157, 0);

	/* MCASP1AXR[0:3] - PADCONFIG 159 to 162 */
    cmb_padMax = 162;
	for (cmb_padCfg = 159; cmb_padCfg <= cmb_padMax; cmb_padCfg++)
	{
		cmb_pinMuxSetMode(cmb_padCfg, CMB_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		cmb_pullSetMode(cmb_padCfg, 0);
	}

	/* MCASP1AXR9 - PADCONFIG 168 */
	cmb_pinMuxSetMode(168, CMB_PADCONFIG_MUX_MODE_QUINARY);
	// set to weak pull down
	cmb_pullSetMode(168, 0);


	/* Configure McASP2 lines */
	/* MCASP2AXR[1:5], MCASP2ACLKR, MCASP2FSR, MCASP2AHCLKR
       - PADCONFIG 140 to 147 */
    cmb_padMax = 147;
	for (cmb_padCfg = 140; cmb_padCfg <= cmb_padMax; cmb_padCfg++)
	{
		cmb_pinMuxSetMode(cmb_padCfg, CMB_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		cmb_pullSetMode(cmb_padCfg, 0);
	}

	/* MCASP2FSX - PADCONFIG 149 */
	cmb_pinMuxSetMode(149, CMB_PADCONFIG_MUX_MODE_QUINARY);
	// set to weak pull down
	cmb_pullSetMode(149, 0);

	/* MCASP2ACLKX - PADCONFIG 151 */
	cmb_pinMuxSetMode(151, CMB_PADCONFIG_MUX_MODE_QUINARY);
	// set to weak pull down
	cmb_pullSetMode(151, 0);

#if 0
	/* Configure GPIO for McASP_CLK_SEL - GPIO0 132 & PADCONFIG 163 */
	cmb_pinMuxSetMode(163, CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SEL_GPIO, CMB_GPIO_OUT);
	/* Slect the clock source as DIR */
	cmb_gpioSetOutput(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SEL_GPIO);

	/* Configure GPIO for McASP_CLK_SEL# - GPIO0 101 & PADCONFIG 110 */
	cmb_pinMuxSetMode(110, CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SELz_GPIO, CMB_GPIO_OUT);
	/* Slect the clock source as DIR */
	//cmb_gpioClearOutput(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SELz_GPIO);
	cmb_gpioSetOutput(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SELz_GPIO);  // Default configurations are set for McASP AHCLK driven by SoC

	/* Configure GPIO for PCM1690_RST# - GPIO1 10 & PADCONFIG 185 */
	cmb_pinMuxSetMode(185, CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_1, CMB_AUDIO_PCM1690_RST_GPIO, CMB_GPIO_OUT);

	cmb_gpioClearOutput(CMB_GPIO_PORT_1, CMB_AUDIO_PCM1690_RST_GPIO);
	cmb_delay(1000);
	cmb_gpioSetOutput(CMB_GPIO_PORT_1, CMB_AUDIO_PCM1690_RST_GPIO);
#endif

	/* Configure McASP AUXCLK source as AUDIO_OSCCLK  */
	hBootCfg->SERIALPORT_CLKCTL = 0;
	hBootCfg->OSC_CTL = 0x200;	//SW2:SW1 = 01 (15 - 30 MHz)

    return (Cmb_EOK);
}

/**
 *  \brief    Resets audio DAC
 *
 *  This function toggles the GPIO signal connected to RST pin
 *  of DAC module to generate DAC reset.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioResetDac(void)
{
	cmb_gpioClearOutput(CMB_GPIO_PORT_1, CMB_AUDIO_PCM1690_RST_GPIO);
	cmb_delay(1000);
	cmb_gpioSetOutput(CMB_GPIO_PORT_1, CMB_AUDIO_PCM1690_RST_GPIO);

	/* Configure McASP AUXCLK source as AUDIO_OSCCLK  */
	hBootCfg->SERIALPORT_CLKCTL = 0;

    return (Cmb_EOK);
}

/**
 *  \brief    Configures audio clock source
 *
 *  McASP can receive clock from DIR module on daughter card or external
 *  I2S device to operate DAC and ADC modules. This function configures
 *  which clock source to use (DIR or I2S) for DAC and ADC operation
 *
 *  \param clkSrc    [IN]  Clock source selection
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioSelectClkSrc(CmbAudioClkSrc clkSrc)
{
	IFPRINT(cmb_write("cmb_AudioSelectClkSrc Called \n"));

	/* Configure GPIO for McASP_CLK_SEL - GPIO0 132 & PADCONFIG 163 */
	cmb_pinMuxSetMode(163, CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SEL_GPIO, CMB_GPIO_OUT);

	/* Configure GPIO for McASP_CLK_SEL# - GPIO0 101 & PADCONFIG 110 */
	cmb_pinMuxSetMode(110, CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SELz_GPIO, CMB_GPIO_OUT);

	if(clkSrc == CMB_AUDIO_CLK_SRC_DIR)
	{
		cmb_gpioSetOutput(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SEL_GPIO);
		cmb_gpioClearOutput(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SELz_GPIO);
	}
	else if(clkSrc == CMB_AUDIO_CLK_SRC_I2S)
	{
		cmb_gpioClearOutput(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SEL_GPIO);
		cmb_gpioSetOutput(CMB_GPIO_PORT_0, CMB_AUDIO_CLK_SELz_GPIO);
	}
	else
	{
		IFPRINT(cmb_write("cmb_AudioSelectClkSrc : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	return (Cmb_EOK);
}

#if (CMB_AUDIO_ADC)

/**
 *  \brief    Initializes ADC module
 *
 *  This function configures the system level setup required for ADC
 *  operation and initializes the ADC module with default values.
 *  This function should be called before calling any other ADC functions.
 *
 *  After executing this function, ADC module will be ready for audio
 *  processing with default configuration. Default ADC configurations
 *  can be changed using the other ADC APIs if required.
 *
 *  \param devId    [IN]  Device ID of ADC HW instance
 *                        Use 'CMB_ADC_DEVICE_ALL' to initialize
 *                        all the ADC devices available.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcInit(CmbAdcDevId devId)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcInit Called \n"));

	if(!(devId <= CMB_ADC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioAdcInit : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xAdcInit(cmb_gAdcI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcInit : ADC Initialization Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_INIT;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Resets ADC module
 *
 *  This function resets all the ADC module registers to their
 *  HW default values.
 *
 *  \param devId    [IN]  Device ID of ADC HW instance
 *                        Use 'CMB_ADC_DEVICE_ALL' to reset
 *                        all the ADC devices available.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcReset(CmbAdcDevId devId)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcReset Called \n"));

	if(!(devId <= CMB_ADC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioAdcReset : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_cmb_pcm186xReset(cmb_gAdcI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcReset : ADC Reset Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Set PLL for ADC module
 *
 *  This function set PLL for all the ADC module registers to their
 *  HW default values.
 *
 *  \param devId    [IN]  Device ID of ADC HW instance
 *                        Use 'CMB_ADC_DEVICE_ALL' to reset
 *                        all the ADC devices available.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcSetPLL(CmbAdcDevId devId)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcSetPLL Called \n"));

	if(!(devId <= CMB_ADC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioAdcSetPLL : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_cmb_pcm186xSetPLL(cmb_gAdcI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcSetPLL : ADC Set PLL Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures ADC gain value
 *
 *  Range of the gain is exposed as percentage by this API. Gain
 *  is indicated as percentage of maximum gain ranging from 0 to 100.
 *  0 indicates minimum gain and 100 indicates maximum gain.
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          Use CMB_ADC_CH_ALL to set gain for all the
 *                          ADC channels
 *
 *  \param gain       [IN]  Gain value; 0 to 100
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcSetGain(CmbAdcDevId  devId,
                                        CmbAdcChanId chanId,
                                        uint8_t   gain)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcSetGain Called \n"));

	if( !( (devId <= CMB_ADC_DEVICE_ALL) &&
	       (chanId <= CMB_ADC_CH_ALL) &&
           (gain <= 100) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcSetGain : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_cmb_pcm186xSetVolume(cmb_gAdcI2cAddr[id], (Uint8)gain,
									 (Uint8)chanId);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcSetGain : ADC Access for Gain Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures ADC analog input selection for left channel
 *
 *  Default input selection of ADC left channels can be modified using
 *  this function.
 *
 *  Default input selection for ADC left channels is listed below
 *    CH1 LEFT  - VINL1
 *    CH2 LEFT  - VINL2
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          CMB_ADC_CH1_LEFT - Input selection for channel 1 left
 *                          CMB_ADC_CH2_LEFT - Input selection for channel 2 left
 *                          CMB_ADC_CH_ALL - Input selection for channel 1 & 2 left
 *
 *  \param inputMux   [IN]  Input mux configuration
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcSetLeftInputMux(CmbAdcDevId        devId,
                                                CmbAdcChanId       chanId,
                                                CmbAdcLeftInputMux inputMux)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcSetLeftInputMux Called \n"));

	if( !( (devId <= CMB_ADC_DEVICE_ALL) &&
	       ((chanId == CMB_ADC_CH1_LEFT) || (chanId == CMB_ADC_CH2_LEFT) ||
	        (chanId == CMB_ADC_CH_ALL)) &&
           ((inputMux <= CMB_ADC_INL_DIFF_VIN1P_VIN1M_VIN4P_VIN4M)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcSetLeftInputMux : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			if(chanId == CMB_ADC_CH_ALL)
			{
				retVal = cmb_cmb_pcm186xInputSel(cmb_gAdcI2cAddr[id], (Uint8)CMB_ADC_CH1_LEFT,
										 (Uint8)inputMux);
				if(retVal)
				{
					IFPRINT(cmb_write("cmb_AudioAdcSetLeftInputMux : ADC Access for Input Selection Failed \n"));
					cmb_errno = CMB_ERRNO_AUDIO_CFG;
					return (Cmb_EFAIL);
				}

				retVal = cmb_cmb_pcm186xInputSel(cmb_gAdcI2cAddr[id], (Uint8)CMB_ADC_CH2_LEFT,
										 (Uint8)inputMux);
			}
			else
			{
				retVal = cmb_cmb_pcm186xInputSel(cmb_gAdcI2cAddr[id], (Uint8)chanId,
										 (Uint8)inputMux);
			}

			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcSetLeftInputMux : ADC Access for Input Selection Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures ADC analog input selection for right channel
 *
 *  Default input selection of ADC right channels can be modified using
 *  this function
 *
 *  Default input selection for ADC right channels is shown below
 *    CH1 RIGHT - VINR1
 *    CH2_RIGHT - VINR2
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                         CMB_ADC_CH1_RIGHT - Input selection for channel 1 right
 *                         CMB_ADC_CH2_RIGHT - Input selection for channel 2 right
 *                         CMB_ADC_CH_ALL - Input selection for channel 1 & 2 right
 *
 *  \param inputMux   [IN]  Input mux configuration
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcSetRightInputMux(CmbAdcDevId         devId,
                                                 CmbAdcChanId        chanId,
                                                 CmbAdcRightInputMux inputMux)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcSetRightInputMux Called \n"));

	if( !( (devId <= CMB_ADC_DEVICE_ALL) &&
	       ((chanId == CMB_ADC_CH1_RIGHT) || (chanId == CMB_ADC_CH2_RIGHT) ||
	        (chanId == CMB_ADC_CH_ALL)) &&
           ((inputMux <= CMB_ADC_INR_DIFF_VIN2P_VIN2M_VIN3P_VIN3M)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcSetRightInputMux : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			if(chanId == CMB_ADC_CH_ALL)
			{
				retVal = cmb_cmb_pcm186xInputSel(cmb_gAdcI2cAddr[id], (Uint8)CMB_ADC_CH1_RIGHT,
										 (Uint8)inputMux);
				if(retVal)
				{
					IFPRINT(cmb_write("cmb_AudioAdcSetRightInputMux : ADC Access for Input Selection Failed \n"));
					cmb_errno = CMB_ERRNO_AUDIO_CFG;
					return (Cmb_EFAIL);
				}

				retVal = cmb_cmb_pcm186xInputSel(cmb_gAdcI2cAddr[id], (Uint8)CMB_ADC_CH2_RIGHT,
										 (Uint8)inputMux);
			}
			else
			{
				retVal = cmb_cmb_pcm186xInputSel(cmb_gAdcI2cAddr[id], (Uint8)chanId,
										 (Uint8)inputMux);
			}

			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcSetRightInputMux : ADC Access for Input Selection Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    ADC audio interface data configuration
 *
 *  This function configures serial audio interface data format and
 *  receive PCM word length for ADC
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param wLen       [IN]  ADC data word length
 *
 *  \param format     [IN]  Audio data format
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcDataConfig(CmbAdcDevId      devId,
                                           CmbAdcRxWordLen  wLen,
                                           CmbAdcDataFormat format)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcDataConfig Called \n"));

	if( !( (devId <= CMB_ADC_DEVICE_ALL) &&
	       ((wLen >= CMB_ADC_RX_WLEN_24BIT) && (wLen <= CMB_ADC_RX_WLEN_16BIT)) &&
           ((format <= CMB_ADC_DATA_FORMAT_TDM_DSP)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcDataConfig : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xDataConfig(cmb_gAdcI2cAddr[id], (Uint8)format,
			                           (Uint8)wLen);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcDataConfig : ADC Access for Data Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Enables/Disables ADC channel mute
 *
 *  This function configures mute functionality of each ADC channel
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          Use CMB_ADC_CH_ALL to apply mute configuration for
 *                          all the ADC channels
 *
 *  \param muteEnable [IN]  Flag to configure mute
 *                          1 - Mute ADC channel
 *                          0 - Unmute ADC channel
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcMuteCtrl(CmbAdcDevId  devId,
                                         CmbAdcChanId chanId,
                                         uint8_t   muteEnable)
{
	CMB_ADC_RET  retVal;
	Uint8    chan;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcMuteCtrl Called \n"));

	if( !( (devId <= CMB_ADC_DEVICE_ALL) &&
	       (chanId <= CMB_ADC_CH_ALL) &&
           ((muteEnable == 0) || (muteEnable == 1)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcMuteCtrl : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	if(chanId == CMB_ADC_CH_ALL)
	{
		chan = 0xF;
	}
	else
	{
		chan = (1 << chanId);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xMuteChannel(cmb_gAdcI2cAddr[id], chan,
			                           (Uint8)muteEnable);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcMuteCtrl : ADC Access for Mute Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures ADC MIC bias
 *
 *  This function enables/disables MIC bias for analog MIC input
 *
 *  \param devId         [IN]  Device ID of ADC HW instance
 *                             Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                             for all the ADC devices available.
 *
 *  \param micBiasEnable [IN]  Mic Bias enable flag
 *                             1 - Enable MIC Bias
 *                             0 - Disable MIC Bias
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcMicBiasCtrl(CmbAdcDevId devId,
                                            uint8_t  micBiasEnable)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcMicBiasCtrl Called \n"));

	if( !( (devId <= CMB_ADC_DEVICE_ALL) &&
           ((micBiasEnable == 0) || (micBiasEnable == 1)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcMicBiasCtrl : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xMicBiasCtrl(cmb_gAdcI2cAddr[id], (Uint8)micBiasEnable);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcMicBiasCtrl : ADC Access for Mic Bias Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures ADC power state
 *
 *  This function enables/disables different power modes supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                           for all the ADC devices available.
 *
 *  \param powState    [IN]  ADC power state to configure
 *
 *  \param stateEnable [IN]  Power state enable flag
 *                           1 - Enables the power state
 *                           0 - Disables the power state
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcConfigPowState(CmbAdcDevId      devId,
                                               CmbAdcPowerState powState,
                                               uint8_t       stateEnable)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcConfigPowState Called \n"));

	if( !( (devId <= CMB_ADC_DEVICE_ALL) &&
	       (powState <= CMB_ADC_POWER_STATE_POWERDOWN) &&
           ((stateEnable == 0) || (stateEnable == 1)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcConfigPowState : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xConfigPowState(cmb_gAdcI2cAddr[id], (Uint8)powState,
			                               (Uint8)stateEnable);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcConfigPowState : ADC Access for Power State Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures ADC interrupts
 *
 *  This function enables/disables different interrupts supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                           for all the ADC devices available.
 *
 *  \param intId       [IN]  Interrupt Id to configure
 *                           Use to 'CMB_ADC_INTR_ALL' to configure all the
 *                           interrupts together
 *
 *  \param intEnable   [IN]  Interrupt enable flag
 *                           1 - Enables the interrupt
 *                           0 - Disables the interrupt
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcConfigIntr(CmbAdcDevId devId,
                                           CmbAdcIntr  intId,
                                           uint8_t  intEnable)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcConfigIntr Called \n"));

	if( !( (devId <= CMB_ADC_DEVICE_ALL) &&
	       (intId <= CMB_ADC_INTR_ALL) &&
           ((intEnable == 0) || (intEnable == 1)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcConfigIntr : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xSetIntr(cmb_gAdcI2cAddr[id], (Uint8)intId,
			                        (Uint8)intEnable);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcConfigIntr : ADC Access for Intr Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Reads ADC interrupt status
 *
 *  This function reads the status of different interrupts supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *
 *  \param intId       [IN]  Interrupt Id to read the status
 *                           Use to 'CMB_ADC_INTR_ALL' to read status of all the
 *                           interrupts together
 *
 *  \return    Interrupt status
 *             1 - Interrupt occurred
 *             0 - No interrupt occurred
 *          0xFF - Error in reading interrupt
 */
uint8_t cmb_AudioAdcGetIntrStatus(CmbAdcDevId devId,
                                      CmbAdcIntr  intId)
{
	Uint8  retVal;

	IFPRINT(cmb_write("cmb_AudioAdcGetIntrStatus Called \n"));

	if( !( ((devId <= CMB_ADC_DEVICE_1)) &&
	       (intId <= CMB_ADC_INTR_ALL) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcGetIntrStatus : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (1);
	}

	retVal = cmb_pcm186xGetIntrStatus(cmb_gAdcI2cAddr[devId]);
	if(retVal == 0xFF)
	{
		IFPRINT(cmb_write("cmb_AudioAdcGetIntrStatus : ADC Access for Intr Status Failed \n"));
		cmb_errno = CMB_ERRNO_AUDIO_STATUS;
		return ((uint8_t)retVal);
	}

	if(intId != CMB_ADC_INTR_ALL)
	{
		retVal = (retVal >> intId) & 0x1;
	}

    return (retVal);
}

/**
 *  \brief    Reads ADC status bits
 *
 *  This function reads the value of different status functions supported
 *  by ADC module (excluding interrupts).
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *
 *  \param status      [IN]  Status function of which status to be read
 *
 *  \return    Value of status function or 0xFF in case of error
 *
 */
uint8_t cmb_AudioAdcGetStatus(CmbAdcDevId  devId,
                                  CmbAdcStatus status)
{
	Uint8  retVal;

	IFPRINT(cmb_write("cmb_AudioAdcGetStatus Called \n"));

	if( !( ((devId <= CMB_ADC_DEVICE_1)) &&
	       (status <= CMB_ADC_STATUS_LDO) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcGetStatus : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (1);
	}

	switch(status)
	{
		/* Current Power State of the device
		 *        0x0 - Power Down
		 *        0x1 - Wait clock stable
		 *        0x2 - Release reset
		 *        0x3 - Stand-by
		 *        0x4 - Fade IN
		 *        0x5 - Fade OUT
		 *        0x9 - Sleep
         *        0xF - Run
		 */
		case CMB_ADC_STATUS_POWER_STATE:
			retVal = cmb_pcm186xGetPowerStateStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* Current Sampling Frequency
		 *       0x0 - Out of range (Low) or LRCK Halt
		 *       0x1 - 8kHz
		 *       0x2 - 16kHz
		 *       0x3 - 32-48kHz
		 *       0x4 - 88.2-96kHz
		 *       0x5 - 176.4-192kHz
		 *       0x6 - Out of range (High)
         *       0x7 - Invalid Fs
		 */
		case CMB_ADC_STATUS_SAMPLING_FREQ:
			retVal = cmb_pcm186xGetSampleFreqStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* Current receiving BCK ratio
		 *        0x0 - Out of range (L) or BCK Halt
		 *        0x1 - 32
		 *        0x2 - 48
		 *        0x3 - 64
		 *        0x4 - 256
		 *        0x6 - Out of range (High)
         *        0x7 - Invalid BCK ratio or LRCK Halt
		 */
		case CMB_ADC_STATUS_BCK_RATIO:
			retVal = cmb_pcm186xGetBckRatioStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* Current SCK Ratio
		 *        0x0 - Out of range (L) or SCK Halt
		 *        0x1 - 128
		 *        0x2 - 256
		 *        0x3 - 384
		 *        0x4 - 512
		 *        0x5 - 768
		 *        0x6 - Out of range (High)
		 *        0x7 - Invalid SCK ratio or LRCK Halt
		 */
		case CMB_ADC_STATUS_SCK_RATIO:
			retVal = cmb_pcm186xGetSckRatioStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* LRCK Halt Status
		 *     0 - No Error
         *     1 - Halt
		 */
		case CMB_ADC_STATUS_LRCK_HALT:
			retVal = cmb_pcm186xGetLrckHltStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* BCK Halt Status
		 *     0 - No Error
         *     1 - Halt
		 */
		case CMB_ADC_STATUS_BCK_HALT:
			retVal = cmb_pcm186xGetBckHltStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* SCK Halt Status
		 *     0 - No Error
         *     1 - Halt
		 */
		case CMB_ADC_STATUS_SCK_HALT:
			retVal = cmb_pcm186xGetSckHltStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* LRCK Error Status
		 *     0 - No Error
		 *     1 - Error
	     */
		case CMB_ADC_STATUS_LRCK_ERR:
			retVal = cmb_pcm186xGetLrckErrStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* BCK Error Status
		 *     0 - No Error
		 *     1 - Error
	     */
		case CMB_ADC_STATUS_BCK_ERR:
			retVal = cmb_pcm186xGetBckErrStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* SCK Error Status
		 *     0 - No Error
		 *     1 - Error
	     */
		case CMB_ADC_STATUS_SCK_ERR:
			retVal = cmb_pcm186xGetSckErrStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* DVDD Status
		 *   0 - Bad/Missing
         *   1 - Good
         */
		case CMB_ADC_STATUS_DVDD:
			retVal = cmb_pcm186xGetDvddStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* AVDD Status
		 *   0 - Bad/Missing
         *   1 - Good
         */
		case CMB_ADC_STATUS_AVDD:
			retVal = cmb_pcm186xGetAvddStatus(cmb_gAdcI2cAddr[devId]);
			break;

		/* Digital LDO Status
		 *   0 - Bad/Missing
         *   1 - Good
         */
		case CMB_ADC_STATUS_LDO:
			retVal = cmb_pcm186xGetLdoStatus(cmb_gAdcI2cAddr[devId]);
			break;

		default:
			retVal = 0xFF;
			break;

	}

	if(retVal == 0xFF)
	{
		IFPRINT(cmb_write("cmb_AudioAdcGetStatus : ADC Access for Get Status Failed \n"));
		cmb_errno = CMB_ERRNO_AUDIO_STATUS;
	}

	return ((uint8_t)retVal);
}

/**
 *  \brief    ADC DSP channel configuration control
 *
 *  This function configures the DSP module processing channels
 *  supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                           for all the ADC devices available.
 *
 *  \param chanCfg     [IN]  DSP channel configuration
 *
 *  \return    Cmb_EOK on Success or error code
 *
 */
Cmb_STATUS cmb_AudioAdcDspCtrl(CmbAdcDevId      devId,
                                        CmbAdcDspChanCfg chanCfg)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcDspCtrl Called \n"));

	if( !( (devId <= CMB_ADC_DEVICE_ALL) &&
	       ((chanCfg == CMB_ADC_DSP_PROC_4CHAN) ||
	        (chanCfg == CMB_ADC_DSP_PROC_2CHAN)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioAdcDspCtrl : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xDspCtrl(cmb_gAdcI2cAddr[id], (Uint8)chanCfg);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcDspCtrl : ADC Access for DSP Chan Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Programs ADC DSP coefficients
 *
 *  ADC module supports an internal DSP which performs additional audio
 *  processing operations like mixing, LPF/HPF etc.
 *  DSP coefficients can be programmed by this function.
 *
 *  \param devId        [IN]  Device ID of ADC HW instance
 *                            Use 'CMB_ADC_DEVICE_ALL' to apply the configuration
 *                            for all the ADC devices available.
 *
 *  \param coeffRegAddr [IN]  Address of DSP coefficient register
 *
 *  \param dspCoeff     [IN]  Value of DSP coefficient
 *                            Lower 24 bits are written to DSP coeff register
 *
 *  \return    Cmb_EOK on Success or error code
 *
 */
Cmb_STATUS cmb_AudioAdcProgDspCoeff(CmbAdcDevId      devId,
                                             uint8_t       coeffRegAddr,
                                             uint32_t      dspCoeff)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcProgDspCoeff Called \n"));

	if(!(devId <= CMB_ADC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioAdcProgDspCoeff : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xProgDspCoeff(cmb_gAdcI2cAddr[id], (Uint8)coeffRegAddr,
			                             (Uint32)dspCoeff);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcProgDspCoeff : ADC DSP Coefficient Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Displays ADC register programmed values
 *
 *  This function is provided for debug purpose to read the value
 *  of ADC registers. Values read from the ADC registers will be displayed
 *  in CCS output window or serial terminal based on the system level
 *  configuration for debug messages.
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'CMB_ADC_DEVICE_ALL' to read the register values
 *                           for all the ADC devices available.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcGetRegDump(CmbAdcDevId devId)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcGetRegDump Called \n"));

	if(!(devId <= CMB_ADC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioAdcGetRegDump : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xRegDump(cmb_gAdcI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcGetRegDump : ADC Reg Read Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_STATUS;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

#endif  /* #if (CMB_AUDIO_ADC) */


#if (CMB_AUDIO_DAC)

/**
 *  \brief    Initializes DAC module
 *
 *  This function configures the system level setup required for DAC
 *  operation and initializes the DAC module with default values.
 *  This function should be called before calling any other DAC functions.
 *
 *  After executing this function, DAC module should be ready for audio
 *  processing with default configuration. Default DAC configurations
 *  can be changed using the other DAC APIs if required.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to initialize
 *                            all the DAC devices available.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacInit(CmbDacDevId devId)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacInit Called \n"));

	if(!(devId <= CMB_DAC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioDacInit : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xDacInit(cmb_gDacI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacInit : DAC Initialization Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_INIT;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Resets DAC module
 *
 *  Resetting the DAC module restarts the re-synchronization between
 *  system clock and sampling clock, and DAC operation.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to reset
 *                            all the DAC devices available.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacReset(CmbDacDevId devId)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacReset Called \n"));

    /* Check input parameters */
	if(!(devId <= CMB_DAC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioDacReset : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			IFPRINT(cmb_write("cmb_AudioDacReset : DAC Reset Failed \n"));
			retVal = cmb_pcm169xReset(cmb_gDacI2cAddr[id]);
			if(retVal)
			{
				cmb_errno = CMB_ERRNO_AUDIO;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures DAC Analog mute control
 *
 *  DAC module supports AMUTE functionality which causes the DAC output
 *  to cut-off from the digital input upon the occurrence of any events
 *  which are configured by AMUTE control.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to apply the configuration
 *                            for all the DAC devices available.
 *
 *  \param muteCtrl     [IN]  Analog mute control event
 *
 *  \param muteEnable   [IN]  Flag to configure AMUTE for given control event
 *                            1 - Enable AMUTE control
 *                            0 - Disable AMUTE control
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacAmuteCtrl(CmbDacDevId     devId,
                                          CmbDacAmuteCtrl muteCtrl,
                                          uint8_t      muteEnable)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacAmuteCtrl Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (muteCtrl <= CMB_DAC_AMUTE_CTRL_DAC_DISABLE_CMD) &&
	       ((muteEnable == 0) || (muteEnable == 1)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacAmuteCtrl : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			if(muteEnable == 1)
			{
                retVal = cmb_pcm169xEnableAmute(cmb_gDacI2cAddr[id],
                                            (Uint8)(1 << muteCtrl));
			}
			else
			{
				retVal = cmb_pcm169xDisableAmute(cmb_gDacI2cAddr[id],
				                             (Uint8)(1 << muteCtrl));
			}

			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacAmuteCtrl : DAC Access for AMUTE Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures DAC Audio sampling mode
 *
 *  By default, DAC module sampling mode is configured for auto mode.
 *  In Auto mode, the sampling mode is automatically set according to multiples
 *  between the system clock and sampling clock. Single rate for 512 fS, 768 fS,
 *  and 1152 fS, dual rate for 256 fS or 384 fS, and quad rate for 128 fS
 *  and 192 fS. Setting the sampling mode is required only if auto mode
 *  configurations are not suitable for the application.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to apply the configuration
 *                            for all the DAC devices available.
 *
 *  \param samplingMode [IN]  DAC audio sampling mode
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacSetSamplingMode(CmbDacDevId        devId,
                                                CmbDacSamplingMode samplingMode)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacSetSamplingMode Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (samplingMode <= CMB_DAC_SAMPLING_MODE_QUAD_RATE) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacSetSamplingMode : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xSetSamplingMode(cmb_gDacI2cAddr[id],
			                                (Uint8)samplingMode);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacSetSamplingMode : DAC Access for Sampling Mode Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures DAC Audio interface data format
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param dataFormat [IN]  DAC audio data format
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacSetDataFormat(CmbDacDevId      devId,
                                              CmbDacDataFormat dataFormat)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacSetDataFormat Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (dataFormat <= CMB_DAC_DATA_FORMAT_24BIT_HS_LEFTJ_TDM) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacSetDataFormat : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xDataConfig(cmb_gDacI2cAddr[id], (Uint8)dataFormat);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacSetDataFormat : DAC Access for Data Format Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures DAC operation mode
 *
 *  This function configures a particular DAC channel pair to be operating
 *  normal or disabled.
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanPair   [IN]  Internal DAC channel pair
 *                          Use CMB_DAC_CHANP_1_2 to CMB_DAC_CHANP_7_8 for
 *                          individual DAC channel pair configuration
 *                          Use CMB_DAC_CHANP_ALL to set operation mode
 *                          for all DAC channels
 *
 *  \param opMode     [IN]  DAC operation mode
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacSetOpMode(CmbDacDevId    devId,
                                          CmbDacChanPair chanPair,
                                          CmbDacOpMode   opMode)
{
	CMB_DAC_RET  retVal;
	Uint8    modeCtrl;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacSetOpMode Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (chanPair <= CMB_DAC_CHANP_ALL) &&
	       ((opMode == CMB_DAC_OPMODE_NORMAL) ||
	        (opMode == CMB_DAC_OPMODE_DISABLED)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacSetOpMode : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	if(chanPair == CMB_DAC_CHANP_ALL)
	{
		modeCtrl = 0xF;
	}
	else
	{
		modeCtrl = (1 << chanPair);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			if(opMode == CMB_DAC_OPMODE_DISABLED)
			{
                retVal = cmb_pcm169xDisableDacOpeartion(cmb_gDacI2cAddr[id], modeCtrl);
			}
			else
			{
				retVal = cmb_pcm169xEnableDacOpeartion(cmb_gDacI2cAddr[id], modeCtrl);
			}

			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacSetOpMode : DAC Access for Opmode Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures DAC filter roll-off
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanPair   [IN]  Internal DAC channel pair
 *                          Use CMB_DAC_CHANP_1_2 to CMB_DAC_CHANP_7_8 for
 *                          individual DAC channel pair configuration
 *                          Use CMB_DAC_CHANP_ALL to set filter roll-off
 *                          for all DAC channels
 *
 *  \param rolloff    [IN]  Roll-off configuration
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacSetFilterRolloff(CmbDacDevId         devId,
                                                 CmbDacChanPair      chanPair,
                                                 CmbDacFilterRolloff rolloff)
{
	CMB_DAC_RET  retVal;
	Uint8    chanId;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacSetFilterRolloff Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (chanPair <= CMB_DAC_CHANP_ALL) &&
	       (rolloff <= CMB_DAC_FILTER_SLOW_ROLLOFF) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacSetFilterRolloff : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	if(chanPair == CMB_DAC_CHANP_ALL)
	{
		chanId = 0xF;
	}
	else
	{
		chanId = (1 << chanPair);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xSetFilterRolloff(cmb_gDacI2cAddr[id], chanId,
			                                 (Uint8)rolloff);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacSetFilterRolloff : DAC Access for Filter Roll-off Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures phase of the DAC analog signal outputs
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanId     [IN]  Internal DAC channel Id
 *                          Use CMB_DAC_CHAN_1 to CMB_DAC_CHAN_8 for individual
 *                          DAC channel configuration
 *                          Use CMB_DAC_CHAN_ALL to set output phase for all
 *                          DAC channels
 *
 *  \param outPhase   [IN]  Output phase selection
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacSetOutputPhase(CmbDacDevId       devId,
                                               CmbDacChanId      chanId,
                                               CmbDacOutputPhase outPhase)
{
	CMB_DAC_RET  retVal;
	Uint8    chan;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacSetOutputPhase Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (chanId <= CMB_DAC_CHAN_ALL) &&
	       (outPhase <= CMB_DAC_OUTPUT_PHASE_INVERTED) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacSetOutputPhase : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	if(chanId == CMB_DAC_CHAN_ALL)
	{
		chan = 0xFF;
	}
	else
	{
		chan = (1 << chanId);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xSetOutputPhase(cmb_gDacI2cAddr[id], chan,
			                               (Uint8)outPhase);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacSetOutputPhase : DAC Access for Output Phase Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Soft mute function control
 *
 *  The Soft mute function allows mute/unmute of DAC output in gradual steps.
 *  This configuration reduces pop and zipper noise during muting of the
 *  DAC output.
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanId     [IN]  Internal DAC channel Id
 *                          Use CMB_DAC_CHAN_1 to CMB_DAC_CHAN_8 for individual
 *                          DAC channel configuration
 *                          Use CMB_DAC_CHAN_ALL to mute/unmute all DAC
 *                          channels
 *
 *  \param muteEnable [IN]  Mute enable flag
 *                          0 for unmute and 1 for mute
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacSoftMuteCtrl(CmbDacDevId   devId,
                                             CmbDacChanId  chanId,
                                             uint8_t    muteEnable)
{
	CMB_DAC_RET  retVal;
	Uint8    chan;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacSoftMuteCtrl Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (chanId <= CMB_DAC_CHAN_ALL) &&
	       ((muteEnable == 0) || (muteEnable == 1)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacSoftMuteCtrl : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	if(chanId == CMB_DAC_CHAN_ALL)
	{
		chan = 0xFF;
	}
	else
	{
		chan = (1 << chanId);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xSoftMuteCtrl(cmb_gDacI2cAddr[id], chan, muteEnable);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacSoftMuteCtrl : DAC Access for Soft Mute Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Sets attenuation mode
 *
 *  DAC module supports two types of volume/attenuation dB range
 *  which can be changed by setting attenuation mode. Volume range and fine
 *  tuning will change based on the attenuation mode.
 *
 *  \param devId    [IN]  Device ID of DAC HW instance
 *                        Use 'DAC_DEVICE_ALL' to apply the configuration
 *                        for all the DAC devices available.
 *
 *  \param attnMode [IN]  Attenuation mode
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacSetAttnMode(CmbDacDevId    devId,
                                            CmbDacAttnMode attnMode)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacSetAttnMode Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (attnMode <= CMB_DAC_ATTENUATION_WIDE_RANGE) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacSetAttnMode : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xSetAttnMode(cmb_gDacI2cAddr[id], (Uint8)attnMode);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacSetAttnMode : DAC Access for Attenuation Mode Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Function to control digital de-emphasis functionality
 *
 *  Disables/Enables the various sampling frequencies of the digital
 *  de-emphasis function.
 *
 *  \param devId     [IN]  Device ID of DAC HW instance
 *                         Use 'DAC_DEVICE_ALL' to apply the configuration
 *                         for all the DAC devices available.
 *
 *  \param deempCtrl [IN]  De-emphasis control options
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacDeempCtrl(CmbDacDevId     devId,
                                          CmbDacDeempCtrl deempCtrl)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacDeempCtrl Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (deempCtrl <= CMB_DAC_DEEMP_32KHZ) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacDeempCtrl : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xDeempCtrl(cmb_gDacI2cAddr[id], (Uint8)deempCtrl);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacDeempCtrl : DAC Access for De-emphasis Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures DAC volume/attenuation
 *
 *  Range of the volume is exposed as percentage by this API. Volume
 *  is indicated as percentage of maximum value ranging from 0 to 100.
 *  0 to mute the volume and 100 to set maximum volume
 *
 *  DAC module supports two types of volume/attenuation dB range
 *  which can be changed using cmb_AudioDacSetAttnMode().
 *  Volume range and fine tuning will change based on the attenuation mode.
 *
 *  \param devId  [IN]  Device ID of DAC HW instance
 *                      Use 'DAC_DEVICE_ALL' to apply the configuration
 *                      for all the DAC devices available.
 *
 *  \param chanId [IN]  Internal DAC channel Id
 *                      Use CMB_DAC_CHAN_1 to CMB_DAC_CHAN_8 for individual
 *                      DAC channel configuration
 *                      Use CMB_DAC_CHAN_ALL to mute/unmute all DAC
 *                      channels
 *
 *  \param volume [IN]  Volume in percentage; 0 to 100
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacSetVolume(CmbDacDevId devId,
                                          CmbDacChanId chanId,
                                          uint8_t  volume)
{
	CMB_DAC_RET  retVal;
	Uint8    id;
	Uint8    chan;

	IFPRINT(cmb_write("cmb_AudioDacSetVolume Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       (chanId <= CMB_DAC_CHAN_ALL) &&
	       (volume <= 100) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacSetVolume : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	if(chanId == CMB_DAC_CHAN_ALL)
	{
		chan = 0xF;
	}
	else
	{
		chan = chanId;
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xSetVolume(cmb_gDacI2cAddr[id], (Uint8)volume, chan);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacSetVolume : DAC Access for Volume Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Configures DAC power-save mode
 *
 *  \param devId     [IN]  Device ID of DAC HW instance
 *                         Use 'DAC_DEVICE_ALL' to apply the configuration
 *                         for all the DAC devices available.
 *
 *  \param PowerMode [IN]  Power-save mode control
 *                         0 - Enable power-save mode
 *                         1 - Disable power-save mode
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacSetPowerMode(CmbDacDevId devId,
                                             uint8_t  PowerMode)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacSetPowerMode Called \n"));

    /* Check input parameters */
	if( !( (devId <= CMB_DAC_DEVICE_ALL) &&
	       ((PowerMode == 0) || (PowerMode == 1)) ) )
	{
		IFPRINT(cmb_write("cmb_AudioDacSetPowerMode : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xSetPowerMode(cmb_gDacI2cAddr[id], PowerMode);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacSetPowerMode : DAC Access for Power-save Mode Config Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_CFG;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Displays DAC register programmed values
 *
 *  This function is provided for debug purpose to read the value
 *  of DAC registers. Values read from the DAC registers will be displayed
 *  in CCS output window or serial terminal based on the system level
 *  configuration for debug messages.
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacGetRegDump(CmbDacDevId devId)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacGetRegDump Called \n"));

	if(!(devId <= CMB_DAC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioDacGetRegDump : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xRegDump(cmb_gDacI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacGetRegDump : DAC Access for Reg Dump Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_STATUS;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}
#endif  /* #if (CMB_AUDIO_DAC) */


#if (CMB_AUDIO_DIR)

/**
 *  \brief    Initializes DIR module
 *
 *  Configures GPIOs and other settings required for DIR module operation.
 *  This function should be called before calling any other DIR functions.
 *
 *  \return    Cmb_EOK on Success or error code
 *
 */
Cmb_STATUS cmb_AudioDirInit(void)
{
	IFPRINT(cmb_write("cmb_AudioDirInit Called \n"));

	/* Configure each SOC_GPIO pin connected to DIR status lines
	   as GPIO and input */

	/* DIR RST signal - GPIO 1, pin 9 */
	cmb_pinMuxSetMode(CMB_AUDIO_DIR_RST_PADCFG,
	              CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_1, CMB_AUDIO_DIR_RST_GPIO, CMB_GPIO_OUT);
	cmb_gpioSetOutput(CMB_GPIO_PORT_1, CMB_AUDIO_DIR_RST_GPIO);

	/* DIR AUDIO signal - GPIO 0, pin 134 */
	cmb_pinMuxSetMode(CMB_AUDIO_DIR_AUDIO_PADCFG,
	              CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_AUDIO_GPIO, CMB_GPIO_IN);

	/* DIR EMPH signal - GPIO 0, pin 135 */
	cmb_pinMuxSetMode(CMB_AUDIO_DIR_EMPH_PADCFG,
	              CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_EMPH_GPIO, CMB_GPIO_IN);

	/* DIR ERROR signal - GPIO 0, pin 136 */
	cmb_pinMuxSetMode(CMB_AUDIO_DIR_ERR_PADCFG,
	              CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_ERR_GPIO, CMB_GPIO_IN);

	/* DIR CLKST signal - GPIO 0, pin 133 */
	cmb_pinMuxSetMode(CMB_AUDIO_DIR_CLKST_PADCFG,
	              CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_CLKST_GPIO, CMB_GPIO_IN);

	/* DIR FSOUT0 signal - GPIO 0, pin 124 */
	cmb_pinMuxSetMode(CMB_AUDIO_DIR_FSOUT0_PADCFG,
	              CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_FSOUT0_GPIO, CMB_GPIO_IN);

	/* DIR FSOUT1 signal - GPIO 0, pin 15 */
	cmb_pinMuxSetMode(CMB_AUDIO_DIR_FSOUT1_PADCFG,
	              CMB_PADCONFIG_MUX_MODE_QUATERNARY);
	cmb_gpioSetDirection(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_FSOUT1_GPIO, CMB_GPIO_IN);

#if 0
    /* Reset DIR */
	cmb_gpioClearOutput(CMB_GPIO_PORT_1, CMB_AUDIO_DIR_RST_GPIO);
	/* DIR Reset signal should be low for at least 100 ns.
	   Adding a delay of 1usec */
	cmb_delay(1);
	cmb_gpioSetOutput(CMB_GPIO_PORT_1, CMB_AUDIO_DIR_RST_GPIO);
#endif

    return (Cmb_EOK);
}

/**
 *  \brief    Resets DIR module
 *
 *  \return    Cmb_EOK on Success or error code
 *
 */
Cmb_STATUS cmb_AudioDirReset(void)
{
	IFPRINT(cmb_write("cmb_AudioDirReset Called \n"));

	cmb_gpioClearOutput(CMB_GPIO_PORT_1, CMB_AUDIO_DIR_RST_GPIO);
	/* DIR Reset signal should be low for at least 100 ns.
	   Adding a delay of 1usec */
	cmb_delay(1);
	cmb_gpioSetOutput(CMB_GPIO_PORT_1, CMB_AUDIO_DIR_RST_GPIO);

    return(Cmb_EOK);
}

/**
 *  \brief    Reads AUDIO output status value of the DIR
 *
 *  DIR AUDIO output pin gives the audio sample word information
 *  of the channel-status data bit 1
 *
 *  \return AUDIO pin output with below possible values
 *  \n      0 - Audio sample word represents linear PCM samples
 *  \n      1 - Audio sample word is used for other purposes
 */
int8_t cmb_AudioDirGetAudioStatus(void)
{
	int8_t audio;

	IFPRINT(cmb_write("cmb_AudioDirGetAudioStatus Called \n"));

	audio = cmb_gpioReadInput(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_AUDIO_GPIO);

    return(audio);
}

/**
 *  \brief    Reads EMPH output status value of the DIR
 *
 *  DIR EMPH output pin gives the emphasis information of the
 *  channel-status data bit 3.
 *
 *  \return EMPH pin output with below possible values
 *  \n      0 - Two audio channels without pre-emphasis
 *  \n      1 - Two audio channels with 50 ms / 15 ms pre-emphasis
 */
int8_t cmb_AudioDirGetEmphStatus(void)
{
	int8_t emph;

	IFPRINT(cmb_write("cmb_AudioDirGetEmphStatus Called \n"));

	emph = cmb_gpioReadInput(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_EMPH_GPIO);

    return(emph);
}

/**
 *  \brief    Reads ERROR pin status value of the DIR
 *
 *  DIR ERROR output pin gives the error state of data and parity errors.
 *
 *  \return EMPH pin output with below possible values
 *  \n      0 - Lock state of PLL and nondetection of parity error
 *  \n      1 - Unlock state of PLL or detection of parity error
 */
int8_t cmb_AudioDirGetErrStatus(void)
{
	int8_t err;

	IFPRINT(cmb_write("cmb_AudioDirGetErrStatus Called \n"));

	err = cmb_gpioReadInput(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_ERR_GPIO);

    return(err);
}

/**
 *  \brief    Reads CLKST pin status value of the DIR
 *
 *  DIR CLKST pin outputs the PLL status change between LOCK and UNLOCK.
 *  The CLKST output pulse depends only on the status change of the PLL.
 *
 *  \return EMPH pin output with below possible values
 *  \n      0 - Lock state of PLL and nondetection of parity error
 *  \n      1 - Unlock state of PLL or detection of parity error
 */
int8_t cmb_AudioDirGetClkStatus(void)
{
	int8_t clk;

	IFPRINT(cmb_write("cmb_AudioDirGetClkStatus Called \n"));

	clk = cmb_gpioReadInput(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_CLKST_GPIO);

    return(clk);
}

/**
 *  \brief    Reads FSOUT[1:0] output status value of the DIR
 *
 *  The DIR module calculates the actual sampling frequency of the
 *  biphase input signal and outputs its result through FSOUT[1:0] pins.
 *
 *  \return FSOUT pin output with below possible values
 *  \n      0 - Calculated Sampling Frequency Output is 43 kHz–45.2 kHz
 *  \n      1 - Calculated Sampling Frequency Output is 46.8 kHz–49.2 kHz
 *  \n      2 - Out of range or PLL unlocked
 *  \n      3 - Calculated Sampling Frequency Output is 31.2 kHz–32.8 kHz
 */
int8_t cmb_AudioDirGetFsOut(void)
{
	int8_t fsout;

	IFPRINT(cmb_write("cmb_AudioDirGetFsOut Called \n"));

	fsout = cmb_gpioReadInput(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_FSOUT1_GPIO);
	fsout <<= 1;
	fsout |= cmb_gpioReadInput(CMB_GPIO_PORT_0, CMB_AUDIO_DIR_FSOUT0_GPIO);

    return(fsout);
}

#endif /* #if (CMB_AUDIO_DIR) */

#endif /* #if (CMB_AUDIO) */
#endif // defined (SOC_K2G)

#if defined (SOC_OMAPL137)
#include "evmOMAPL137/include/evmc674x_pinmux.h"
#include "evmOMAPL137/include/evmc674x_i2c.h"
#include "evmc674x_audio_adc.h"
#include "evmc674x_audio_dac.h"

/* DAC HW instance I2C slave address */
Uint8 cmb_gDacI2cAddr[CMB_AUDIO_DAC_COUNT] = {CMB_AUDIO_DAC0_ADDR,
                                               CMB_AUDIO_DAC1_ADDR};
/* ADC HW instance I2C slave address */
Uint8 cmb_gAdcI2cAddr[CMB_AUDIO_CMB_ADC_COUNT] = {CMB_AUDIO_ADC0_ADDR,
                                               CMB_AUDIO_ADC1_ADDR};

uint32_t cmb_errno = 0;
/******************************************************************************
 * cmb_delay
 ******************************************************************************/
#define OMAPL137_C674X_MHZ (465)
Cmb_STATUS cmb_delay(uint32_t usecs)
{
	int32_t delayCount = (int32_t) usecs * OMAPL137_C674X_MHZ;
	int32_t start_val  = (int32_t) CSL_chipReadTSCL();

	while (((int32_t)CSL_chipReadTSCL() - start_val) < delayCount);

	return Cmb_EOK;
}

Cmb_STATUS cmb_AudioInit(void)
{
	configMcaspPinMux();

	return (Cmb_EOK);
}

/**
 *  \brief    Initializes ADC module
 *
 *  This function configures the system level setup required for ADC
 *  operation and initializes the ADC module with default values.
 *  This function should be called before calling any other ADC functions.
 *
 *  After executing this function, ADC module will be ready for audio
 *  processing with default configuration. Default ADC configurations
 *  can be changed using the other ADC APIs if required.
 *
 *  \param devId    [IN]  Device ID of ADC HW instance
 *                        Use 'CMB_ADC_DEVICE_ALL' to initialize
 *                        all the ADC devices available.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioAdcInit(CmbAdcDevId devId)
{
	CMB_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioAdcInit Called \n"));

	if(!(devId <= CMB_ADC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioAdcInit : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_ADC_DEVICE_0; id < CMB_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_ADC_DEVICE_ALL))
		{
			retVal = cmb_pcm186xAdcInit(cmb_gAdcI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioAdcInit : ADC Initialization Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_INIT;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}

/**
 *  \brief    Initializes DAC module
 *
 *  This function configures the system level setup required for DAC
 *  operation and initializes the DAC module with default values.
 *  This function should be called before calling any other DAC functions.
 *
 *  After executing this function, DAC module should be ready for audio
 *  processing with default configuration. Default DAC configurations
 *  can be changed using the other DAC APIs if required.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to initialize
 *                            all the DAC devices available.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS cmb_AudioDacInit(CmbDacDevId devId)
{
	CMB_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(cmb_write("cmb_AudioDacInit Called \n"));

	if(!(devId <= CMB_DAC_DEVICE_ALL))
	{
		IFPRINT(cmb_write("cmb_AudioDacInit : Invalid Inputs\n"));
		cmb_errno = CMB_ERRNO_INVALID_ARGUMENT;
		return (Cmb_EINVALID);
	}

	for (id = CMB_DAC_DEVICE_0; id < CMB_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == CMB_DAC_DEVICE_ALL))
		{
			retVal = cmb_pcm169xDacInit(cmb_gDacI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(cmb_write("cmb_AudioDacInit : DAC Initialization Failed \n"));
				cmb_errno = CMB_ERRNO_AUDIO_INIT;
				return (Cmb_EFAIL);
			}
		}
	}

    return (Cmb_EOK);
}
#endif // defined (SOC_OMAPL137)

/* Nothing past this point */
