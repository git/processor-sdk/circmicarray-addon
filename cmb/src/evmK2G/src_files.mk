#
# This file is the makefile for building GPIO RTOS library.
#
#   Copyright (c) Texas Instruments Incorporated 2017
#

SRCDIR += . src/$(BOARD)
INCDIR += . src/$(BOARD)/include

# Source files for $(BOARD)
ifeq ($(BOARD),$(filter $(BOARD), evmK2G))
SRCS_COMMON += evmc66x_pinmux.c
SRCS_COMMON += evmc66x_i2c.c
SRCS_COMMON += evmc66x_gpio.c

PACKAGE_SRCS_COMMON += makefile include/cmb.h cmb_component.mk \
                      src/$(BOARD)/evmc66x_pinmux.c \
                      src/$(BOARD)/evmc66x_i2c.c \
                      src/$(BOARD)/evmc66x_gpio.c \
	 	      build/makefile.mk \
	 	      src/$(BOARD)/src_files.mk                      
endif

