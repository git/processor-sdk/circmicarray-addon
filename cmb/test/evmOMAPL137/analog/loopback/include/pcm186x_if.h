/**
 * \file       pcm186x_if.h
 *
 * \brief       The macro definitions and function prototypes for
 *              configuring PCM186x ADC
 *
 *  This file contains the implementation of the PCM186x ADC driver for
 *  DSP BIOS operating system.
 *
 *  (C) Copyright 2017, Texas Instruments, Inc
 */

/*
* Copyright (C) 2017 Texas Instruments Incorporated - http://www.ti.com/
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _PCM186X_IF_H_
#define _PCM186X_IF_H_

/**************************************************************************
**                       Macro Definitions
**************************************************************************/
/*
** Macros for the clock tree for the audio input and output
*/
// AUXCLK master mode
// AUXCLK (24Mhz, master) --> McASP1 (RX) --> PCM1864 (CMB U1/U2, slave)
// AUXCLK (24Mhz, master) --> McASP1 (TX) --> AIC3106 (slave)
#define AUXCLK_MASTER_MODE

/*
** Macros for the dataType variable to pass to AIC31DataConfig function
*/
#define PCM186X_DATATYPE_I2S             (0u << 6u) /* I2S Mode */
#define PCM186X_DATATYPE_DSP             (1u << 6u) /* DSP Mode */
#define PCM186X_DATATYPE_RIGHTJ          (2u << 6u) /* Right Aligned Mode */
#define PCM186X_DATATYPE_LEFTJ           (3u << 6u) /* Left Aligned Mode */

/**************************************************************************
**                      API function Prototypes
**************************************************************************/
extern void PCM186XReset(unsigned int baseAddr);
extern void PCM186XADCInit(void);

#endif

/***************************** End Of File ***********************************/
