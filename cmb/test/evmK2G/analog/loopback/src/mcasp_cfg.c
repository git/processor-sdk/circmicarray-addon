/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      mcasp_config.c
 *
 * \brief     Configures McASP module
 *
 */

#include "string.h"
#include "mcasp_cfg.h"
#include "cmb.h"

#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>

/* Frame index for Rx and Tx buffers */
uint8_t rxFrameIndex = 1;
uint8_t txFrameIndex = 1;
uint32_t gtxFrameIndexCount = 0;
uint32_t grxFrameIndexCount = 0;

/* Flags for counting Rx and Tx interrupts */
volatile uint32_t rxFlag = 0;
volatile uint32_t txFlag = 0;

/* Semaphore handle for Tx and Rx */
Semaphore_Handle semR;
Semaphore_Handle semT;
Semaphore_Params params;

/* McASP device handles */
Ptr hMcaspDevTx;
Ptr hMcaspDevRx;

/* McASP channel handles */
Ptr hMcaspTxChan;
Ptr hMcaspRxChan;

/* McASP channel parameters */
Mcasp_Params mcaspTxParams;
Mcasp_Params mcaspRxParams;

/* McASP Callback function argument */
uint32_t txChanMode;
uint32_t rxChanMode;
uint32_t mcaspRxChanArg = 1;
uint32_t mcaspTxChanArg = 2;

/* McASP Tx and Rx frame buffers */
MCASP_Packet rxFrame[NUM_BUFS];
MCASP_Packet txFrame[NUM_BUFS];

/* McASP Tx and Rx frame buffer pointers */
Ptr txBuf[NUM_BUFS];
Ptr rxBuf[NUM_BUFS];

/* Error flag */
uint32_t gblErrFlag = 0;
Error_Block eb;

/* Handle to heap object */
extern HeapMem_Handle heapHandle;

/* External function declarations */
void GblErr(int arg);
signed char*  getGlobalAddr(signed char* addr);

#define TX_FIFO_EVENT_DMA_RATIO  1
#define RX_FIFO_EVENT_DMA_RATIO  1
/* McASP HW setup for receive */
Mcasp_HwSetupData mcaspRcvSetup = {
        /* .rmask    = */ 0xFFFFFFFF, /* 16 bits are to be used     */
        /* .rfmt     = */ 0x000180F0, /*
                                       * 1 bit delay from framesync
                                       * MSB first
                                       * No extra bit padding
                                       * Padding bit (ignore)
                                       * slot Size is 32
                                       * Reads from DMA port
                                       * NO rotation
                                       */
        /* .afsrctl  = */ 0X00000111, /* I2S mode - 2 slot TDM
                                       * Frame sync is one word
                                       * Externally generated frame sync
                                       * Falling edge is start of frame
                                       */
        /* .rtdm     = */ 0x00000003, /* slot 1 and 2 are active (I2S)        */
        /* .rintctl  = */ 0x00000000, /* sync error and overrun error         */
        /* .rstat    = */ 0x000001FF, /* reset any existing status bits       */
        /* .revtctl  = */ 0x00000000, /* DMA request is enabled               */
        {
             /* .aclkrctl  = */ 0x00000080, /* Bit CLK Pol: falling edge, ACLKR is external */
             /* .ahclkrctl = */ 0x00000000, /* AHCLKR is external */
             /* .rclkchk   = */ 0x00000000
        }
};

/* McASP HW setup for transmit */
#if (CMB_AUDIO_DAC)
Mcasp_HwSetupData mcaspXmtSetup = {
        /* .xmask    = */ 0xFFFFFFFF, /* 16 bits are to be used     */
        /* .xfmt     = */ 0x000180F0, /*
                                       * 1 bit delay from framesync
                                       * MSB first
                                       * No extra bit padding
                                       * Padding bit (ignore)
                                       * slot Size is 32
                                       * Reads from DMA port
                                       * NO rotation
                                       */
        /* .afsxctl  = */ 0x00000113, /* I2S mode - 2 slot TDM
                                       * Frame sync is one word
                                       * Falling edge is start of frame
                                       * Externally generated frame sync
                                       */
        /* .xtdm     = */ 0x00000003, /* slot 1 and 2 are active (I2S)               */
        /* .xintctl  = */ 0x00000000, /* sync error,overrun error,clK error   */
        /* .xstat    = */ 0x000001FF, /* reset any existing status bits       */
        /* .xevtctl  = */ 0x00000000, /* DMA request is enabled or disabled   */
        {
             /* .aclkxctl  = */ 0X000000E3, /* Bit CLK Pol: falling edge, ASYNC is 1, ACLKX is internal, HF CLK to BCLK divider is 4 */
             /* .ahclkxctl = */ 0x00000000, /* AHCLKX is external */
             /* .xclkchk   = */ 0x00000000
        },

};
#endif

/* McAsp channel parameters for receive                      */
Mcasp_ChanParams  mcaspRxChanParam =
{
	0x0004,                    /* number of serializers      */
	{Mcasp_SerializerNum_0,
	 Mcasp_SerializerNum_1,
	 Mcasp_SerializerNum_2,
	 Mcasp_SerializerNum_3 }, /* serializer index           */
	&mcaspRcvSetup,
	TRUE,
	Mcasp_OpMode_TDM,          /* Mode (TDM/DIT)             */
	Mcasp_WordLength_32,
	NULL,
	0,
	NULL,
	(Mcasp_GblCallback)GblErr,
	2,                        /* number of TDM channels      */
	///Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1,
	Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_2,
	TRUE,
	RX_FIFO_EVENT_DMA_RATIO,
	TRUE,
	Mcasp_WordBitsSelect_LSB
};

#if (CMB_AUDIO_DAC)
/* McAsp channel parameters for transmit             */
Mcasp_ChanParams  mcaspTxChanParam =
{
	0x0001,                   /* number of serializers       */
	{Mcasp_SerializerNum_2,}, /* serializer index for DAC0    */
	&mcaspXmtSetup,
	TRUE,
	Mcasp_OpMode_TDM,
	Mcasp_WordLength_32,      /* word width                  */
	NULL,
	0,
	NULL,
	(Mcasp_GblCallback)GblErr,
	2,                        /* number of TDM channels      */
	Mcasp_BufferFormat_1SER_MULTISLOT_INTERLEAVED,
	///Mcasp_BufferFormat_1SER_MULTISLOT_NON_INTERLEAVED,
	TRUE,
	TX_FIFO_EVENT_DMA_RATIO,
	TRUE,
	Mcasp_WordBitsSelect_LSB
};
#endif

/* Handle to eDMA */
extern EDMA3_DRV_Handle hEdma1;

/**
 *  \brief    Function called by McASP driver in case of error
 *
 *  \return    None
 */
void GblErr(int arg)
{
	gblErrFlag = 1;
}

/**
 *  \brief   McASP callback function called up on the data transfer completion
 *
 *  \param  arg   [IN]  - Application specific callback argument
 *  \param  ioBuf [IN]  - McASP IO buffer
 *
 *  \return    None
 */
void mcaspAppCallback(void *arg, MCASP_Packet *ioBuf)
{
	/* Callback is triggered by Rx completion */
	if(ioBuf->cmd == MCASP_READ)
	{
		rxFlag++;

		if(rxFrameIndex == 0)
		{
			rxFrameIndex = 1;
		}
		else
		{
			rxFrameIndex = 0;
		}

		/* Post semaphore */
		Semaphore_post(semR);
	}

	/* Callback is triggered by Tx completion */
	if(ioBuf->cmd == MCASP_WRITE)
	{
		if(txFrameIndex == 0)
		{
			txFrameIndex = 1;
		}
		else
		{
			txFrameIndex = 0;
		}

		txFlag++;

		/* Post semaphore */
		Semaphore_post(semT);
	}
}

/**
 *  \brief   Initializes McASP data buffers and submits to McASP driver
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS initBuffers(void)
{
	Error_Block  eb;
    uint32_t     count = 0;
    IHeap_Handle iheap;
    Int          status;

    iheap = HeapMem_Handle_to_xdc_runtime_IHeap(heapHandle);
    Error_init(&eb);

    /* Allocate buffers for the McASP data exchanges */
    for(count = 0; count < NUM_BUFS; count++)
    {
        rxBuf[count] = Memory_calloc(iheap, (BUFSIZE * RX_NUM_SERIALIZER),
        						     BUFALIGN, &eb);
        if(NULL == rxBuf[count])
        {
            IFPRINT(cmb_write("\r\nMEM_calloc failed for Rx\n"));
        }
    }

#if (CMB_AUDIO_DAC)
    /* Allocate buffers for the McASP data exchanges */
    for(count = 0; count < NUM_BUFS; count++)
    {
        txBuf[count] = Memory_calloc(iheap, (BUFSIZE * TX_NUM_SERIALIZER),
        							 BUFALIGN, &eb);
        if(NULL == txBuf[count])
        {
            IFPRINT(cmb_write("\r\nMEM_calloc failed for Tx\n"));
        }
    }
#endif
    for(count = 0; count < NUM_BUFS; count++)
    {
        /* Issue the first & second empty buffers to the input stream */
    	memset((uint8_t *)rxBuf[count], 0xAA, (BUFSIZE * RX_NUM_SERIALIZER));

		/* RX frame processing */
		rxFrame[count].cmd    = MCASP_READ;
		rxFrame[count].addr   = (void*)(getGlobalAddr(rxBuf[count]));
		rxFrame[count].size   = BUFSIZE * RX_NUM_SERIALIZER;
		rxFrame[count].arg    = (uint32_t) mcaspRxChanArg;
		rxFrame[count].status = 0;
		rxFrame[count].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		/* Submit McASP packet for Rx */
		status = mcaspSubmitChan(hMcaspRxChan, &rxFrame[count]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			IFPRINT(cmb_write("mcaspSubmitChan for Rx Failed\n"));
			return (Cmb_EFAIL);
		}
    }

#if (CMB_AUDIO_DAC)
    for(count = 0; count < NUM_BUFS; count++)
    {
    	memset((uint8_t *)txBuf[count], 0xCC, (BUFSIZE * TX_NUM_SERIALIZER));

		/* TX frame processing */
		txFrame[count].cmd    = MCASP_WRITE;
		txFrame[count].addr   = (void*)(getGlobalAddr(txBuf[count]));
		txFrame[count].size   = BUFSIZE * TX_NUM_SERIALIZER;
		txFrame[count].arg    = (uint32_t) mcaspTxChanArg;
		txFrame[count].status = 0;
		txFrame[count].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		/* Submit McASP packet for Tx */
		status = mcaspSubmitChan(hMcaspTxChan, &txFrame[count]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			IFPRINT(cmb_write("mcaspSubmitChan for Tx Failed\n"));
			return (Cmb_EFAIL);
		}
    }
#endif
    return (Cmb_EOK);
}

/**
 *  \brief   Configures McASP module and creates the channel
 *           for audio Tx and Rx
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS mcaspAudioConfig(void)
{
	Int status;

#if (CMB_AUDIO_DAC)
	hMcaspDevTx  = NULL;
	hMcaspTxChan = NULL;
#endif
	hMcaspDevRx  = NULL;
	hMcaspRxChan = NULL;

	/* Initialize McASP Tx and Rx parameters */
#if (CMB_AUDIO_DAC)
	mcaspTxParams = Mcasp_PARAMS;
#endif
	mcaspRxParams = Mcasp_PARAMS;

#if (CMB_AUDIO_DAC)
	mcaspTxParams.mcaspHwSetup.tx.clk.clkSetupClk = 0x63;
	mcaspTxParams.mcaspHwSetup.rx.clk.clkSetupClk = 0x23;
#endif
	mcaspRxParams.mcaspHwSetup.rx.clk.clkSetupClk = 0x23;
	mcaspRxParams.mcaspHwSetup.tx.clk.clkSetupClk = 0x63;

#if (CMB_AUDIO_DAC)
	mcaspTxParams.mcaspHwSetup.glb.pdir |= 0x2000000; //Set Amute pin as output for Tx channel
#endif

	/* Initialize eDMA handle */
	mcaspRxChanParam.edmaHandle  = hEdma1;
#if (CMB_AUDIO_DAC)
	mcaspTxChanParam.edmaHandle = hEdma1;

	/* Bind McASP2 for Tx */
	status = mcaspBindDev(&hMcaspDevTx, CSL_MCASP_2, &mcaspTxParams);
	if((status != MCASP_COMPLETED) || (hMcaspDevTx == NULL))
	{
		IFPRINT(cmb_write("mcaspBindDev for Tx Failed\n"));
		return (Cmb_EFAIL);
	}
#endif
	/* Bind McASP1 for Rx */
	status = mcaspBindDev(&hMcaspDevRx, CSL_MCASP_1, &mcaspRxParams);
	if((status != MCASP_COMPLETED) || (hMcaspDevRx == NULL))
	{
		IFPRINT(cmb_write("mcaspBindDev for Rx Failed\n"));
		return (Cmb_EFAIL);
	}

#if (CMB_AUDIO_DAC)
	/* Create McASP channel for Tx */
	status = mcaspCreateChan(&hMcaspTxChan, hMcaspDevTx,
	                         MCASP_OUTPUT,
	                         &mcaspTxChanParam,
	                         mcaspAppCallback, &txChanMode);
	if((status != MCASP_COMPLETED) || (hMcaspTxChan == NULL))
	{
		IFPRINT(cmb_write("mcaspCreateChan for Tx Failed\n"));
		return (Cmb_EFAIL);
	}

	configAudioDAC();
#endif

	/* Create McASP channel for Rx */
	status = mcaspCreateChan(&hMcaspRxChan, hMcaspDevRx,
	                         MCASP_INPUT,
	                         &mcaspRxChanParam,
	                         mcaspAppCallback, &rxChanMode);
	if((status != MCASP_COMPLETED) || (hMcaspRxChan == NULL))
	{
		IFPRINT(cmb_write("mcaspCreateChan for Rx Failed\n"));
		return (Cmb_EFAIL);
	}

	/* Initialize the buffers and submit for McASP Tx/Rx */
	if(initBuffers() != Cmb_EOK)
	{
		IFPRINT(cmb_write("McASP Buffer Initialization Failed\n"));
		return (Cmb_EFAIL);
	}

	return (Cmb_EOK);
}

/**
 *  \brief   Function to exit the test
 *
 *  \return    None
 */
void testRet(uint32_t status)
{
	cmb_write("\n\nAudio DC Analog Interface Test Completed!\n");

	testExit(status);
}

/**
 *  \brief   Task to echo the input data to output
 *
 *  Waits for the McASP data transfer completion and copies the
 *  Rx data to Tx buffers
 *
 *  \return    Cmb_EOK on Success or error code
 */
#define DUMP_SEC			10
#define FRAME_PER_SEC		100
int gCmbDumpBufIdx = 0;
unsigned char gCmbDumpBuf[(BUFSIZE*RX_NUM_SERIALIZER)*FRAME_PER_SEC*DUMP_SEC];
unsigned char gCmbOutDumpBuf[(BUFSIZE*TX_NUM_SERIALIZER)*FRAME_PER_SEC*DUMP_SEC];
Void Audio_echo_Task(void)
{
    int32_t i;
    unsigned char *tempTxPtr, *tempRxPtr;

    Semaphore_Params_init(&params);

	/* Create semaphores to wait for buffer reclaiming */
    semR = Semaphore_create(0, &params, &eb);
    semT = Semaphore_create(0, &params, &eb);

    /* Forever loop to continuously receive and transmit audio data */
    while (1)
    {
    	if(gblErrFlag)
    	{
    		break;
		}

    	/* Reclaim full buffer from the input stream */
    	Semaphore_pend(semR, BIOS_WAIT_FOREVER);
#if (CMB_AUDIO_DAC)
    	Semaphore_pend(semT, BIOS_WAIT_FOREVER);
#endif
        /* Reclaim full buffer from the input stream */
#if (CMB_AUDIO_DAC)
    	gtxFrameIndexCount = txFrameIndex;
#endif
    	grxFrameIndexCount = rxFrameIndex;

		///for(count = 0; count < 0; count++)
		///{
		///	asm("* Comment to maintain loops through compiler optimization");
		///}

#if 1
        if (gCmbDumpBufIdx<FRAME_PER_SEC*DUMP_SEC)
        {
        	// copy all 8 channels (L/R channels for all 4 serializers)
			///memcpy((Ptr)(&gCmbDumpBuf[gCmbDumpBufIdx*BUFSIZE*RX_NUM_SERIALIZER]), rxBuf[grxFrameIndexCount], (BUFSIZE * RX_NUM_SERIALIZER));

			// copy first channel (L channel for the first serializer)
			///memcpy((Ptr)(&gCmbDumpBuf[gCmbDumpBufIdx*(BUFSIZE/2)]), rxBuf[grxFrameIndexCount], (BUFSIZE/2));

        	///memcpy((Ptr)(&gCmbOutDumpBuf[gCmbDumpBufIdx*(BUFSIZE)]), txBuf[gtxFrameIndexCount], (BUFSIZE));
			// copy 4 L channels (L channels for all 4 serializers)
			///memcpy((Ptr)(&gCmbDumpBuf[gCmbDumpBufIdx*BUFSIZE*RX_NUM_SERIALIZER/2]), rxBuf[grxFrameIndexCount], (BUFSIZE * RX_NUM_SERIALIZER/2));
        	// copy 4 R channels (R channels for all 4 serializers)
			///memcpy((Ptr)(&gCmbDumpBuf[gCmbDumpBufIdx*BUFSIZE*RX_NUM_SERIALIZER/2]), rxBuf[grxFrameIndexCount]+(BUFSIZE*RX_NUM_SERIALIZER/2), (BUFSIZE * RX_NUM_SERIALIZER/2));
			gCmbDumpBufIdx++;
        }
#else
        if (gCmbDumpBufIdx<=FRAME_PER_SEC*DUMP_SEC)
        {
        	// copy L channel for first serializer
			///memcpy((Ptr)(&gCmbDumpBuf[gCmbDumpBufIdx*BUFSIZE/2]), rxBuf[grxFrameIndexCount], (BUFSIZE/2));
        	// copy L channel for second serializer
			///memcpy((Ptr)(&gCmbDumpBuf[gCmbDumpBufIdx*BUFSIZE/2]), (rxBuf[grxFrameIndexCount]+(BUFSIZE/2)), (BUFSIZE/2));
        	// copy R channel for first serializer
			memcpy((Ptr)(&gCmbDumpBuf[gCmbDumpBufIdx*BUFSIZE/2]), (rxBuf[grxFrameIndexCount]+(BUFSIZE * RX_NUM_SERIALIZER/2)), (BUFSIZE/2));
        	// copy R channel for first serializer
			///memcpy((Ptr)(&gCmbDumpBuf[gCmbDumpBufIdx*BUFSIZE/2]), (rxBuf[grxFrameIndexCount]+(BUFSIZE * RX_NUM_SERIALIZER/2)+(BUFSIZE/2)), (BUFSIZE/2));
			gCmbDumpBufIdx++;
        }
#endif

		// print "Test Passed", if 500 audio frames have been processed
		if (gCmbDumpBufIdx==500)
		{
			cmb_write("\nTest Passed\n");
			UART_printf("\nTest Passed\n");
		}
		
        /* Copy the receive information to the transmit buffer */
#if (CMB_AUDIO_DAC)
#if 0	// Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1
    	// copy RX mic 1 to TX left channel and RX mic 5 to right channel
		// set the RX pointer to mic 1
    	tempRxPtr = (uint32_t *)rxBuf[grxFrameIndexCount];
    	// set the TX pointer to left cahhnel
		tempTxPtr = (uint32_t *)txBuf[gtxFrameIndexCount];
		// copy RX mic 1 to TX left channel
		for (i=0; i<BUFLEN/2; i++)
    	{
			// copy the left channel of first serializer to the left channel of TX buffer
			*tempTxPtr = *tempRxPtr;
    		tempTxPtr++;
			// copy the left channel of swecond serializer to the right channel of TX buffer
			*tempTxPtr = *(tempRxPtr+BUFLEN+2);
    		tempTxPtr++;
    		tempRxPtr += RX_NUM_SERIALIZER*2;
    	}

#else   // Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_2
		// copy the L/R from the first serializer to TX buffer
    	///memcpy(txBuf[gtxFrameIndexCount], rxBuf[grxFrameIndexCount], BUFSIZE);

    	// copy RX mic 1 to TX left channel and RX mic 5 to right channel
		// set the RX pointer to mic 1
    	tempRxPtr = rxBuf[grxFrameIndexCount];
    	// set the TX pointer to left cahhnel
		tempTxPtr = txBuf[gtxFrameIndexCount];
		// copy RX mic 1 to TX left channel
		for (i=0; i<BUFLEN/2; i++)
    	{
			// copy the left channel of first serializer (mic1) to the left channel of TX buffer
			memcpy(tempTxPtr, tempRxPtr, 4);

    		tempTxPtr += 4;

    		// copy the right channel of first serializer (mic2) to the right channel of TX buffer
			///memcpy(tempTxPtr, tempRxPtr+4, 4);

    		// copy the right channel of third serializer (mic3) to the right channel of TX buffer
			///memcpy(tempTxPtr, tempRxPtr+BUFSIZE*2+4, 4);

    		// copy the left channel of third serializer (mic4) to the right channel of TX buffer
			///memcpy(tempTxPtr, tempRxPtr+BUFSIZE*2, 4);

			// copy the left channel of second serializer (mic5) to the right channel of TX buffer
			///memcpy(tempTxPtr, tempRxPtr+BUFSIZE, 4);

    		// copy the right channel of second serializer (mic6) to the right channel of TX buffer
			///memcpy(tempTxPtr, tempRxPtr+BUFSIZE+4, 4);

    		// copy the right channel of fourth serializer (mic7) to the right channel of TX buffer
			///memcpy(tempTxPtr, tempRxPtr+BUFSIZE*3+4, 4);

    		// copy the left channel of fourth serializer (mic8) to the right channel of TX buffer
			memcpy(tempTxPtr, tempRxPtr+BUFSIZE*3, 4);

    		tempTxPtr += 4;

    		tempRxPtr += RX_NUM_SERIALIZER*2;
    	}
#endif
#endif

#if (CMB_AUDIO_DAC)
        /* Issue full buffer to the output stream                             */
        /* TX frame processing */
		txFrame[gtxFrameIndexCount].cmd    = MCASP_WRITE;
		txFrame[gtxFrameIndexCount].addr   = (void*)(getGlobalAddr(txBuf[gtxFrameIndexCount]));
		txFrame[gtxFrameIndexCount].size   = (BUFSIZE * TX_NUM_SERIALIZER);
		txFrame[gtxFrameIndexCount].arg    = (uint32_t) mcaspTxChanArg;
		txFrame[gtxFrameIndexCount].status = 0;
		txFrame[gtxFrameIndexCount].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		mcaspSubmitChan(hMcaspTxChan, &txFrame[gtxFrameIndexCount]);
#endif

        /* Issue an empty buffer to the input stream                          */
		rxFrame[grxFrameIndexCount].cmd    = MCASP_READ;
		rxFrame[grxFrameIndexCount].addr   = (void*)(getGlobalAddr(rxBuf[grxFrameIndexCount]));
		rxFrame[grxFrameIndexCount].size   = (BUFSIZE * RX_NUM_SERIALIZER);
		rxFrame[grxFrameIndexCount].arg    = (uint32_t) mcaspRxChanArg;
		rxFrame[grxFrameIndexCount].status = 0;
		rxFrame[grxFrameIndexCount].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

        mcaspSubmitChan(hMcaspRxChan, &rxFrame[grxFrameIndexCount]);
	}

    testRet(0);
}

/* Nothing past this point */
