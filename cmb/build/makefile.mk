#
# This file is the makefile for building CMB RTOS library.
#
#   Copyright (c) Texas Instruments Incorporated 2017
#
include $(PDK_INSTALL_PATH)/ti/build/Rules.make
ifeq ($(BOARD), evmK2G)
include $(PDK_CMB_COMP_PATH)/src/$(BOARD)/src_files.mk
endif
ifeq ($(BOARD), evmOMAPL137)
include $(PDK_CMB_COMP_PATH)/src/$(BOARD)/src_files.mk
endif
include $(PDK_CMB_COMP_PATH)/src/src_files_cmb.mk

MODULE_NAME = cmb

# List all the external components/interfaces, whose interface header files
#  need to be included for this component
INCLUDE_EXTERNAL_INTERFACES = pdk board gpio i2c mcasp

CFLAGS_LOCAL_COMMON = $(PDK_CFLAGS)

# Include common make files
ifeq ($(MAKERULEDIR), )
#Makerule path not defined, define this and assume relative path from ROOTDIR
  MAKERULEDIR := $(ROOTDIR)/ti/build/makerules
  export MAKERULEDIR
endif
include $(MAKERULEDIR)/common.mk

# Nothing beyond this point
