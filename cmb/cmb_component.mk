#*******************************************************************************
#                                                                              *
# Copyright (c) 2017 Texas Instruments Incorporated - http://www.ti.com/       *
#                        ALL RIGHTS RESERVED                                   *
#                                                                              *
#*******************************************************************************

# File: cmb_component.mk
#       This file is component include make file of cmb addon library.
# List of variables set in this file and their purpose:
# <mod>_RELPATH        - This is the relative path of the module, typically from
#                        top-level directory of the package
# <mod>_PATH           - This is the absolute path of the module. It derives from
#                        absolute path of the top-level directory (set in env.mk)
#                        and relative path set above
# <mod>_INCLUDE        - This is the path that has interface header files of the
#                        module. This can be multiple directories (space separated)
# <mod>_PKG_LIST       - Names of the modules (and sub-modules) that are a part
#                        part of this module, including itself.
# <mod>_BOARD_DEPENDENCY - "yes": means the code for this module depends on
#                             board and the compiled obj/lib has to be kept
#                             under <board> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no board dependent code and hence
#                             the obj/libs are not kept under <board> dir.
# <mod>_CORE_DEPENDENCY     - "yes": means the code for this module depends on
#                             core and the compiled obj/rtos has to be kept
#                             under <core> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no core dependent code and hence
#                             the obj/rtoss are not kept under <core> dir.
# <mod>_APP_STAGE_FILES     - List of source files that belongs to the module
#                             <mod>, but that needs to be compiled at application
#                             build stage (in the context of the app). This is
#                             primarily for link time configurations or if the
#                             source file is dependent on options/defines that are
#                             application dependent. This can be left blank or
#                             not defined at all, in which case, it means there
#                             no source files in the module <mod> that are required
#                             to be compiled in the application build stage.
#
ifeq ($(cmb_component_make_include), )

# under other list
cmb_BOARDLIST       = evmK2G evmOMAPL137
cmb_SOCLIST         = k2g omapl137
cmb_k2g_CORELIST    = c66x
cmb_omapl137_CORELIST    = c674x

############################
# cmb package
# List of components included under cmb rtos
# The components included here are built and will be part of cmb rtos
############################
cmb_LIB_LIST = cmb
cmb_EXAMPLE_LIST =

#
# CMB Modules
#

# CMB LIB
cmb_COMP_LIST = cmb
cmb_RELPATH = ti/addon/cmb/$(BOARD)
cmb_PATH = $(PDK_CMB_COMP_PATH)
cmb_LIBNAME = ti.addon.cmb
export cmb_LIBNAME
cmb_LIBPATH = $(cmb_PATH)/lib
export cmb_LIBPATH
cmb_OBJPATH = $(cmb_RELPATH)/cmb/$(BOARD)
export cmb_OBJPATH
cmb_MAKEFILE = -f build/makefile.mk
export cmb_MAKEFILE
cmb_BOARD_DEPENDENCY = no
cmb_CORE_DEPENDENCY = no
cmb_SOC_DEPENDENCY = yes
export cmb_COMP_LIST
export cmb_BOARD_DEPENDENCY
export cmb_CORE_DEPENDENCY
export cmb_SOC_DEPENDENCY
cmb_PKG_LIST = cmb
export cmb_PKG_LIST
cmb_INCLUDE = $(cmb_PATH)
export cmb_BOARDLIST
export cmb_SOCLIST
export cmb_$(SOC)_CORELIST

#
# CMB Examples
#

# CMBaudio loopback app
cmb_audio_loopback_app_COMP_LIST = cmb_audio_loopback_app
ifeq ($(BOARD),$(filter $(BOARD), evmOMAPL137))
cmb_audio_loopback_app_RELPATH = ti/addon/cmb/test/$(BOARD)/analog/loopback/make
cmb_audio_loopback_app_PATH = $(PDK_CMB_COMP_PATH)/test/$(BOARD)/analog/loopback/make
endif
ifeq ($(BOARD),$(filter $(BOARD), evmK2G))
cmb_audio_loopback_app_RELPATH = ti/addon/cmb/test/$(BOARD)/analog/loopback/build/make
cmb_audio_loopback_app_PATH = $(PDK_CMB_COMP_PATH)/test/$(BOARD)/analog/loopback/build/make
endif
cmb_audio_loopback_app_BOARD_DEPENDENCY = yes
cmb_audio_loopback_app_CORE_DEPENDENCY = no
cmb_audio_loopback_app_XDC_CONFIGURO = no
export cmb_audio_loopback_app_COMP_LIST
export cmb_audio_loopback_app_BOARD_DEPENDENCY
export cmb_audio_loopback_app_CORE_DEPENDENCY
export cmb_audio_loopback_app_XDC_CONFIGURO
cmb_audio_loopback_app_PKG_LIST = cmb_audio_loopback_app
cmb_audio_loopback_app_INCLUDE = $(cmb_audio_loopback_app_PATH) $(PDK_CMB_COMP_PATH)/include
cmb_audio_loopback_app_BOARDLIST = evmK2G evmOMAPL137
export cmb_audio_loopback_app_BOARDLIST
cmb_audio_loopback_app_$(SOC)_CORELIST = $(cmb_$(SOC)_CORELIST)
export cmb_audio_loopback_app_$(SOC)_CORELIST
cmb_EXAMPLE_LIST += cmb_audio_loopback_app

export cmb_LIB_LIST
export cmb_EXAMPLE_LIST

cmb_component_make_include := 1
endif
